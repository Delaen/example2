"use strict";

$(document).on('click','.ajax-form-button', function(e) {
    let modal = $('#modal');
    modal.find('#modalContent').html('').load($(this).attr('value'));
    modal.modal('show');

    $('.modal-header h4','#modal').html($(this).data('header'));
});


$(document).on('beforeSubmit','#modal_form', function(e) {
    e.preventDefault();

    let form = $(this);

    $.ajax({
        type: "POST",
        url: form.attr("action"),
        showNoty: false,
        data: form.serialize(),
        success: function (res) {
            if (res && res == 'success') {
                location.reload();
            } else {
                $('#modal').modal('show')
                    .find('#modalContent')
                    .html(res);
            }
        },
        error: function(){
            alert('Error!');
        }
    });
    return false;
});