<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use kartik\select2\Select2;
use app\models\constants\Constant;

/* @var $this yii\web\View */
/* @var $searchModel app\models\catalog\search\ModelWorkSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Доп.расходы';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-work-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::button('Добавить расход', [
            'class' => 'btn btn-success ajax-form-button',
            'value' => Url::to(['cost/create']),
            'data' => ['header' => 'Добавить расход']
        ]); ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute' => 'date',
                'filter' => \kartik\date\DatePicker::widget([
                    'model' => $searchModel,
                    'value' => $searchModel->date,
                    'name' => 'CostSearch[date]',
                    'size' => 'xs',
                    'options' => ['placeholder' => 'Выберите...'],
                    'pluginOptions' => [
                        'orientation' => 'bottom left',
                        'format' => 'dd.mm.yyyy',
                        'todayHighlight' => true
                    ]
                ])
            ],
           [
                   'attribute' => 'workshop',
               'filter' => Constant::workshopsFilter(true),
               'visible' => (Yii::$app->user->identity->getRole() != \app\models\constants\Roles::MASTER),
               'value' => function($model) {
                    return $model->workshopDescription;
               }
           ],
           [
                   'attribute' => 'cost_catalog_id',
                    'filter' => Select2::widget([
                       'name' => 'CostSearch[cost_catalog_id]',
                       'model' => $searchModel,
                       'data' => \app\models\catalog\Cost::ddl(),
                       'value' => $searchModel->cost_catalog_id,
                       'options' => ['multiple' => false, 'placeholder' => 'Выберите...'],
                       'pluginOptions' => ['allowClear' => true]
                   ]),
               'value' => function($model) {
                    return $model->name;
               }

           ],
            'quantity',
            'comment:raw',

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                            'class' => 'ajax-form-button',
                            'value' => Url::to(['cost/update', 'id' => $model->id]),
                            'data' => ['header' => 'Редактировать']
                        ]);

                    },
                    'view' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                            'class' => 'ajax-form-button',
                            'value' => Url::to(['cost/view', 'id' => $model->id]),
                            'data' => ['header' => 'Просмотр']
                        ]);

                    }
                ],
                'visibleButtons' => [
                    'update' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN),
                    'view' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN),
                    'delete' => Yii::$app->user->can(\app\models\constants\Roles::CEO),
                ]
            ],
        ],
    ]); ?>

    <?php
    Modal::begin([
        'header' => '<h4></h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>
</div>
