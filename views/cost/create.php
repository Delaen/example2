<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\catalog\ModelWork */

$this->title = 'Добавить расход';
$this->params['breadcrumbs'][] = ['label' => 'Доп.расходы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-work-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
