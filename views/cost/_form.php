<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\catalog\ModelWork */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model-work-form">

    <?php $form = ActiveForm::begin(['id' => 'modal_form', 'options' => ['enableAjaxValidation' => true]]); ?>

    <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(), [
        'options' => ['placeholder' => 'Выберите дату'],
        'pluginOptions' => [
            'orientation' => 'bottom',
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'cost_catalog_id')->widget(Select2::classname(), [
        'data' => \app\models\catalog\Cost::ddl(),
        'options' => ['placeholder' => 'Выберите расход'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php if (Yii::$app->user->identity->getRole() != \app\models\constants\Roles::MASTER): ?>
        <?= $form->field($model, 'workshop')->dropDownList(\app\models\constants\Constant::workshopsFilter(true)) ?>
    <?php endif; ?>

    <?= $form->field($model, 'quantity')->textInput(['type' => 'number', 'step' => 0.1]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
