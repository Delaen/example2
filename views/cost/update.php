<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\catalog\ModelWork */

$this->title = 'Редактировать: ' . $model->name . "($model->id)";
$this->params['breadcrumbs'][] = ['label' => 'Доп.расходы', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактировать';
?>
<div class="model-work-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
