<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\catalog\ModelWork */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Типовые работы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="model-work-view">


    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            [
                'attribute' => 'created',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'attribute' => 'updated',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
        ],
    ]) ?>

</div>
