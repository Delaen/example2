<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\catalog\ModelWork */

$this->title = 'Добавить работу';
$this->params['breadcrumbs'][] = ['label' => 'Типовые работы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="model-work-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
