<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\constants\Constant;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\helpers\StockHelper;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ElectricitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Крошка';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="electricity-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <h3><b>Остаток на складе:</b></h3>

    <?php if (Yii::$app->user->identity->getRole() != \app\models\constants\Roles::MASTER): ?>
        <div class="row">
            <?php foreach (Constant::workshopsFilter() as $workshop): ?>
                <div class="col-md-4">
                    <?= StockHelper::rubberCrumbStockHtml($workshop); ?>
                </div>
            <?php endforeach; ?>
        </div>
    <?php else: ?>
        <div class="row">
            <div class="col-md-4">
                <?= StockHelper::rubberCrumbStockHtml(Yii::$app->user->identity->user->workshop); ?>
            </div>
        </div>
    <?php endif; ?>

    <p>
        <?= Html::button('Добавить крошку', [
            'class' => 'btn btn-success ajax-form-button',
            'value' => Url::to(['stock/rubber-crumb/receipt/create']),
            'data' => ['header' => 'Добавить крошку']
        ]); ?>

        <?= Html::button('Добавить отгрузку', [
            'class' => 'btn btn-success ajax-form-button',
            'value' => Url::to(['stock/rubber-crumb/deduction/create']),
            'data' => ['header' => 'Добавить отгрузку']
        ]); ?>
    </p>

    <h4 style="margin: 40px 0 15px"><b>История движения</b></h4>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => '№'],
            [
                'label' => 'Дата',
                'value' => function ($model) {
                    return date('d.m.Y', strtotime($model['date']));
                }
            ],
            [
                'label' => 'Цех',
                'value' => function ($model) {
                    return Constant::workshops(true)[$model['workshop']];
                }
            ],
            [
                'label' => 'Движение',
                'value' => function ($model) {
                    return ($model['type'] === Constant::FLOW_RECEIPT) ? 'Сделано' : 'Отгружено';
                }
            ],
            [
                'label' => 'Фракция, мм',
                'value' => function ($model) {
                    return Constant::rubberCrumbFractions(true)[$model['fraction']];
                }
            ],
            [
                'label' => 'Количество, кг',
                'format' => 'decimal',
                'value' => function ($model) {
                    return $model['quantity'];
                }
            ],
            [
                'label' => 'Смена',
                'format' => 'html',
                'value' => function ($model) {
                    return ($model['type'] === Constant::FLOW_RECEIPT) && $model['source'] ?
                        Html::a('Смена №' . $model['source'], ['work-shift/view', 'id' => $model['source']]) :
                        null;
                }
            ],
            [
                'label' => 'Заказ',
                'format' => 'html',
                'value' => function ($model) {
                    return ($model['type'] === 'deduction') && $model['source'] ?
                        Html::a('Заказ №' . $model['source'], ['order/view', 'id' => $model['source']]) :
                        null;
                }
            ],
            [
                'label' => 'Примечание',
                'format' => 'text',
                'value' => function ($model) {
                    return $model['comment'];
                }
            ],
            ['class' => 'yii\grid\ActionColumn', 'template' => '{delete}',
                'buttons' => [
                    'delete' => function ($url, $model) {
                        $url = Url::to(['stock/rubber-crumb/deduction/delete', 'id' => $model['id']]);
                        if ($model['type'] === Constant::FLOW_RECEIPT) {
                            $url = Url::to(['stock/rubber-crumb/receipt/delete', 'id' => $model['id']]);
                        }
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'data' => ['method' => 'post', 'confirm' => 'Вы уверены?']
                        ]);

                    },
                ],
                'visibleButtons' => [
                        'delete' => function ($model, $key, $index) {
                            return ($model['type'] == Constant::FLOW_DEDUCTION) || (!$model['source']);
                        }
                ],
            ],
        ],
    ]); ?>

    <?php
    Modal::begin([
        'header' => '<h4></h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>

</div>
