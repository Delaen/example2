<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\flows\rubberCrumb\Receipt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model-work-form">

    <?php $form = ActiveForm::begin(['id' => 'modal_form', 'options' => ['enableAjaxValidation' => true]]); ?>

    <div class="row">
        <div class="col-md-6">
            <?php if (!$workShiftId): ?>
                <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(), [
                    'options' => ['placeholder' => 'Выберите дату'],
                    'pluginOptions' => [
                        'orientation' => 'bottom',
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                        'todayHighlight' => true
                    ]
                ]); ?>
            <?php endif; ?>
        </div>
    </div>

    <?php if (!$workShiftId && Yii::$app->user->identity->getRole() != \app\models\constants\Roles::MASTER): ?>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'workshop')->dropDownList(\app\models\constants\Constant::workshopsFilter(true)) ?>
            </div>
        </div>
    <?php endif; ?>

    <p><b>Фракции:</b></p>
    <div class="row">
        <div class="col-md-4"><?= $form->field($model, 'fraction0_05')->textInput(['number', 'step' => 0.1]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'fraction0_08')->textInput(['number', 'step' => 0.1]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'fraction05_2')->textInput(['number', 'step' => 0.1]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'fraction1_3')->textInput(['number', 'step' => 0.1]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'fraction2_4')->textInput(['number', 'step' => 0.1]) ?></div>
        <div class="col-md-4"><?= $form->field($model, 'fraction3_5')->textInput(['number', 'step' => 0.1]) ?></div>
    </div>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
