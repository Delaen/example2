<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\flows\rubberCrumb\Deduction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="deduction-form">

    <?php $form = ActiveForm::begin(['id' => 'modal_form', 'options' => ['enableAjaxValidation' => true]]); ?>

    <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(), [
        'options' => ['placeholder' => 'Выберите дату'],
        'pluginOptions' => [
            'orientation' => 'bottom',
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'quantity')->textInput(['type' => 'number', 'step' => 0.01]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
