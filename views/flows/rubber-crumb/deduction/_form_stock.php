<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\flows\rubberCrumb\Deduction */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model-work-form">

    <?php $form = ActiveForm::begin(['id' => 'modal_form', 'options' => ['enableAjaxValidation' => true]]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(), [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'orientation' => 'bottom',
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>
    </div>

    <?php if (Yii::$app->user->identity->getRole() != \app\models\constants\Roles::MASTER): ?>
        <div class="row">
            <div class="col-md-4">
                <?= $form->field($model, 'workshop')->dropDownList(\app\models\constants\Constant::workshopsFilter(true)) ?>
            </div>
        </div>
    <?php endif; ?>

    <div id="order-fraction" style="display: none">
        <div class="row">
            <div class="col-md-4"><?= $form->field($model, 'fraction')->textInput(['number', 'step' => 0.1]) ?></div>
        </div>
    </div>
    <div id="fractions-area">
        <p><b>Фракции:</b></p>
        <div class="row">
            <div class="col-md-4"><?= $form->field($model, 'fraction0_05')->textInput(['number', 'step' => 0.1]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction0_08')->textInput(['number', 'step' => 0.1]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction05_2')->textInput(['number', 'step' => 0.1]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction1_3')->textInput(['number', 'step' => 0.1]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction2_4')->textInput(['number', 'step' => 0.1]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction3_5')->textInput(['number', 'step' => 0.1]) ?></div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'order_id')->widget(Select2::classname(), [
                'data' => $orders,
                'options' => ['placeholder' => 'Выберите заказ'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]); ?>
        </div>
    </div>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$js = <<<JS
 let fractionsArea = $('#fractions-area'); 
 let orderFraction = $('#order-fraction'); 

  $('#deductionform-order_id').on('change', function(event) {
     if($(this).val()) {
         $('#deductionform-fraction0_1, #deductionform-fraction1_3, #deductionform-fraction3_5').val('');
         orderFraction.slideDown();
         fractionsArea.slideUp();
     }else {
         $('#deductionform-fraction').val('');
         orderFraction.slideUp();
         fractionsArea.slideDown();
     }
  });
  
JS;
$this->registerJs($js);
?>
