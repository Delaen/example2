<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\flows\rubberCrumb\CashReceipt */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Cash Receipts', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cash-receipt-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'order_id',
            'date',
            'purposeDescription',
            'recipientDescription',
            'paymentTypeDescription',
            'summary',
            'comment:ntext',
            'created',
            'updated',
        ],
    ]) ?>

</div>
