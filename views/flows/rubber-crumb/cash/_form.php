<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\constants\Constant;

/* @var $this yii\web\View */
/* @var $model app\models\flows\rubberCrumb\CashReceipt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cash-receipt-form">

    <?php $form = ActiveForm::begin(['id' => 'modal_form', 'options' => ['enableAjaxValidation' => true]]); ?>

    <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(), [
        'options' => ['placeholder' => 'Выберите дату'],
        'pluginOptions' => [
            'orientation' => 'bottom',
            'autoclose'=>true,
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true
        ]
    ]); ?>

    <?= $form->field($model, 'purpose')->dropDownList(Constant::purposes(true)) ?>

    <?= $form->field($model, 'recipient')->dropDownList(Constant::organizations(true)) ?>

    <?= $form->field($model, 'payment_type')->dropDownList(Constant::paymentTypes(true)) ?>

    <?= $form->field($model, 'summary')->textInput(['type' => 'number', 'step' => 0.01]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
