<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\catalog\Employee;
use kartik\select2\Select2;
use app\models\catalog\ModelWork;

/* @var $this yii\web\View */
/* @var $model app\models\flows\rubberCrumb\Receipt */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="model-salary">

    <?php $form = ActiveForm::begin(['id' => 'modal_form', 'options' => ['enableAjaxValidation' => true]]); ?>

    <?= $form->field($model, 'employee_id')->widget(Select2::classname(), [
        'data' => Employee::ddl(),
        'options' => ['placeholder' => 'Выберите сотрудника'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'salary')->textInput(['type' => 'number', 'step' => 0.01]) ?>

    <?= $form->field($model, 'additional_work_id')->widget(Select2::classname(), [
        'data' => ModelWork::ddlWithNoSelect(),
        'options' => ['placeholder' => 'Выберите....'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'additional_work_salary')->textInput(['type' => 'number', 'step' => 0.01]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
