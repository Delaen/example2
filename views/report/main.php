<?php

use app\models\flows\rubberCrumb\Salary;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\constants\Constant;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    [
        'label' => 'Дата',
        'value' => function ($model) {
            return date('d.m.Y', strtotime($model['date']));
        },
        'footer' => '<b>Итого:</b>',
    ],
    [
        'class' => \app\widgets\TotalColumn::class,
        'label' => 'З/п, руб.',
        'value' => function ($model) {
            return $model['salary'] ? Yii::$app->formatter->asDecimal($model['salary']) : 0;
        },
        'footerOptions' => ['class' => 'text-bold'],
    ],
    [
        'class' => \app\widgets\TotalColumn::class,
        'label' => 'Сделано крошки, кг',
        'value' => function ($model) {
            return $model['receipt'] ? Yii::$app->formatter->asDecimal($model['receipt']) : 0;
        },
          'footerOptions' => ['class' => 'text-bold'],
    ],
    [
        'class' => \app\widgets\TotalColumn::class,
        'label' => 'Отгружено крошки, кг',
        'value' => function ($model) {
            return $model['deduction'] ? Yii::$app->formatter->asDecimal($model['deduction']) : 0;
        },
          'footerOptions' => ['class' => 'text-bold'],
    ],
    [
        'class' => \app\widgets\TotalColumn::class,
        'label' => 'З/п за доп.работы',
        'value' => function ($model) {
            return $model['addSalary'] ? Yii::$app->formatter->asDecimal($model['addSalary']) : 0;
        },
          'footerOptions' => ['class' => 'text-bold'],
    ],
];


?>
<div class="electricity-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ExportMenu::widget([
        'dataProvider' => new \yii\data\SqlDataProvider([
            'sql' => $dataProvider->sql,
            'pagination' => false,
        ]),
        //'filterModel' => false,
        'showColumnSelector' => false,
        'filename' => 'Report_main_ ' . date('Y_m_d'),
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::FORMAT_EXCEL_X => [
                'label' => 'Сохранить в Excel',
                'iconOptions' => ['class' => 'hidden'],
                'linkOptions' => ['class' => 'btn btn-info'],
                'options' => ['style' => 'list-style-type:none; display:inline-block', 'class' => 'pull-right'],
            ],
        ],
        'asDropdown' => false,
        'showFooter' => true,
        'showPageSummary' => true,
        'columns' => $columns,
    ]); ?>

    <?php $form = ActiveForm::begin(['method' => 'get']) ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($searchModel, 'dateFrom')->widget(\kartik\date\DatePicker::className(), [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'orientation' => 'bottom',
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($searchModel, 'dateTo')->widget(\kartik\date\DatePicker::className(), [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'orientation' => 'bottom',
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>

        <div class="col-md-2">
            <?= Html::submitButton('Фильтр', ['class' => 'btn btn-success mt-23']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($searchModel, 'workshop')->dropDownList(Constant::workshops(true)) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => 'asdgfsd'],
        'summary' => false,
        'showFooter' => true,
        'columns' => $columns,
    ]); ?>

</div>
