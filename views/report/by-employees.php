<?php

use app\models\flows\rubberCrumb\Salary;
use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\constants\Constant;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Отчет по сотрудникам';
$this->params['breadcrumbs'][] = $this->title;

$totalSalary = (clone $dataProvider->query)->select('sum(salary)')->column();
$totalSalary = $totalSalary[0] === null ? 0 : $totalSalary[0];

$totalAdditionalSalary = (clone $dataProvider->query)->select('sum(additional_work_salary)')->column();
$totalAdditionalSalary = $totalAdditionalSalary[0] === null ? 0 : $totalAdditionalSalary[0];

$columns = [
    [
        'attribute' => 'workShift.date',
        'footer' => '<b>Итого:</b>'
    ],
    'employee.fio',
    [
        'class' => \app\widgets\TotalColumn::class,
        'attribute' => 'salary',
        'format' => 'currency',
        'footerOptions' => [
            'class' => 'text-bold'
        ],
        //значение футера для страницы выставляется через TotalColumn, для excel - через аттриубт footer
        'footer' => $totalSalary
    ],
    [
        'class' => \app\widgets\TotalColumn::class,
        'attribute' => 'additional_work_salary',
        'format' => 'currency',
        'footerOptions' => [
            'class' => 'text-bold'
        ],
        //значение футера для страницы выставляется через TotalColumn, для excel - через аттриубт footer
        'footer' => $totalAdditionalSalary
    ],
];


?>
<div class="electricity-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= ExportMenu::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => $dataProvider->query,
            'pagination' => false,
        ]),
        //'filterModel' => false,
        'showColumnSelector' => false,
        'filename' => 'Report_by_employees_ ' . date('Y_m_d'),
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::FORMAT_EXCEL_X => [
                'label' => 'Сохранить в Excel',
                'iconOptions' => ['class' => 'hidden'],
                'linkOptions' => ['class' => 'btn btn-info'],
                'options' => ['style' => 'list-style-type:none; display:inline-block', 'class' => 'pull-right'],
            ],
        ],
        'asDropdown' => false,
        'showFooter' => true,
        'showPageSummary' => true,
        'columns' => $columns,
    ]); ?>

    <?php $form = ActiveForm::begin(['method' => 'get']) ?>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($searchModel, 'dateFrom')->widget(\kartik\date\DatePicker::className(), [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'orientation' => 'bottom',
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($searchModel, 'dateTo')->widget(\kartik\date\DatePicker::className(), [
                'options' => ['placeholder' => 'Выберите дату'],
                'pluginOptions' => [
                    'orientation' => 'bottom',
                    'autoclose' => true,
                    'format' => 'dd.mm.yyyy',
                    'todayHighlight' => true
                ]
            ]); ?>
        </div>

        <div class="col-md-2">
            <?= Html::submitButton('Фильтр', ['class' => 'btn btn-success mt-23']) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-3">
            <?= $form->field($searchModel, 'workshop')->dropDownList(Constant::workshops(true)) ?>
        </div>
    </div>

    <?php ActiveForm::end(); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'summary' => false,
        'showFooter' => true,
        'columns' => $columns,
    ]); ?>

    <h3><b>Всего:</b></h3>
    <table class="table table-striped table-bordered">
        <tr>
            <td><b>Всего з/п, руб.</b></td>
            <td><?= Yii::$app->formatter->asDecimal($totalSalary) ?></td>
        </tr>
        <tr>
            <td><b>Всего з/п за доп. работу, руб.</b></td>
            <td><?= Yii::$app->formatter->asDecimal($totalAdditionalSalary) ?></td>
        </tr>
    </table>

</div>
