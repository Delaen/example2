<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\catalog\Cost */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Расходы (справочник)', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cost-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'unit',
            [
                'attribute' => 'created',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'attribute' => 'updated',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
        ],
    ]) ?>

</div>
