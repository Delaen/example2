<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\catalog\search\CostSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Расходы (справочник)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cost-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=Html::button('Добавить', [
            'class' => 'btn btn-success ajax-form-button',
            'value' => Url::to(['cost-catalog/create']),
            'data' => ['header' => 'Добавить расход']
        ]);?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
           // ['class' => 'yii\grid\SerialColumn'],

            'id',
            'name',
            'unit',
            [
                'attribute' => 'created',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'attribute' => 'updated',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                            'class' => 'ajax-form-button',
                            'value' => Url::to(['cost-catalog/update', 'id' => $model->id]),
                            'data' => ['header' => 'Редактировать']
                        ]);

                    },
                    'view' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                            'class' => 'ajax-form-button',
                            'value' => Url::to(['cost-catalog/view', 'id' => $model->id]),
                            'data' => ['header' => 'Просмотр: ' . $model->name]
                        ]);

                    }
                ]
            ],
        ],
    ]); ?>

    <?php
    Modal::begin([
        'header' => '<h4>Добавить типовую работу</h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>

</div>
