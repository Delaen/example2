<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\constants\Constant;
use unclead\multipleinput\MultipleInput;
use app\models\catalog\Employee;

/* @var $this yii\web\View */
/* @var $model app\models\WorkShift */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="work-shift-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'date')->widget(\kartik\date\DatePicker::className(), [
        'options' => ['placeholder' => 'Выберите дату'],
        'pluginOptions' => [
            'orientation' => 'bottom',
            'autoclose' => true,
            'format' => 'dd.mm.yyyy',
            'todayHighlight' => true
        ]
    ]); ?>

    <?php if (Yii::$app->user->identity->getRole() != \app\models\constants\Roles::MASTER):?>
        <?= $form->field($model, 'workshop')->dropDownList(\app\models\constants\Constant::workshopsFilter(true)) ?>
    <?php endif;?>

    <?= $form->field($model, 'type')->dropDownList(Constant::workShifts(true)) ?>

    <div class="form-group" style="background-color: #dddddd; padding: 15px">
        <h3><b>Фракция: </b></h3>
        <div class="row">
            <div class="col-md-4"><?= $form->field($model, 'fraction0_05', [
                    'template' => '<div class="col-md-3">{label}</div><div class="col-md-9">{input}</div>{error}'
                ])->textInput(['type' => 'number', 'step' => 0.01]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction0_08', [
                    'template' => '<div class="col-md-3">{label}</div><div class="col-md-9">{input}</div>{error}'
                ])->textInput(['type' => 'number', 'step' => 0.01]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction05_2', [
                    'template' => '<div class="col-md-3">{label}</div><div class="col-md-9">{input}</div>{error}'
                ])->textInput(['type' => 'number', 'step' => 0.01]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction1_3', [
                    'template' => '<div class="col-md-3">{label}</div><div class="col-md-9">{input}</div>{error}'
                ])->textInput(['type' => 'number', 'step' => 0.01]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction2_4', [
                    'template' => '<div class="col-md-3">{label}</div><div class="col-md-9">{input}</div>{error}'
                ])->textInput(['type' => 'number', 'step' => 0.01]) ?></div>
            <div class="col-md-4"><?= $form->field($model, 'fraction3_5', [
                    'template' => '<div class="col-md-3">{label}</div><div class="col-md-9">{input}</div>{error}'
                ])->textInput(['type' => 'number', 'step' => 0.01]) ?></div>
        </div>

        <?= $form->field($model, 'salaries')->widget(MultipleInput::className(), [
            'min' => 0,
            'max' => 40,
            'allowEmptyList' => true,
            'enableGuessTitle' => true,
            'addButtonPosition' => MultipleInput::POS_HEADER, // show add button in the header
            'columns' => [
                [
                    'name' => 'employee_id',
                    'title' => 'Сотрудник',
                    'type' => \kartik\select2\Select2::className(),
                    'options' => [
                        'data' => Employee::ddl(),
                        'options' => ['multiple' => false, 'placeholder' => 'Выберите...'],
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                    ],
                    'defaultValue' => 1
                ],
                [
                    'name' => 'salary',
                    'title' => 'З/п за крошку',
                    'defaultValue' => 0,
                    'options' => ['type' => 'number', 'step' => 0.01]
                ],
                [
                    'name' => 'additional_work_id',
                    'title' => 'Доп.работы',
                    'type' => \kartik\select2\Select2::className(),
                    'options' => [
                        'data' => $additionalWorks,
                        'options' => ['multiple' => false, 'placeholder' => 'Выберите...'],
                        'pluginOptions' => [
                            'allowClear' => false
                        ],
                    ],
                    'defaultValue' => null

                ],
                [
                    'name' => 'comment',
                    'title' => 'Примечание'
                ],
                [
                    'name' => 'additional_work_salary',
                    'title' => 'З/п за доп.работы',
                    'defaultValue' => 0,
                    'options' => ['type' => 'number', 'step' => 0.01]
                ],
            ]
        ])
            ->label(false); ?>
    </div>


    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
