<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkShift */

$this->title = 'Добавить смену';
$this->params['breadcrumbs'][] = ['label' => 'Смены (крошка)', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-shift-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_create_form', compact('model', 'additionalWorks')) ?>

</div>
