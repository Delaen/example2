<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\WorkShift */

$this->title = $model->shortDescription;
$this->params['breadcrumbs'][] = ['label' => 'Смены (крошка)', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="work-shift-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
       <?=Html::button('Редактировать', [
           'class' => 'ajax-form-button btn btn-primary',
           'value' => Url::to(['work-shift/update', 'id' => $model->id]),
           'data' => ['header' => 'Редактировать']
       ]);
       ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Вы уверены?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            [
                'attribute' => 'id',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            'date',
            'workshopDescription',
            [
                'attribute' => 'type',
                'filter' => \app\models\constants\Constant::workShifts(true),
                'value' => function ($model) {
                    return $model->typeDescription;
                }
            ],
            'goodsTypeDescription',
            'totalSalarySum:currency',
            [
                'attribute' => 'created',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'attribute' => 'updated',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
        ],
    ]) ?>

    <?= Yii::$app->user->can(\app\models\constants\Roles::CEO) ? Html::button('<span class="glyphicon glyphicon-plus-sign" style="font-size: 20px"></span>', [
        'class' => 'btn btn-success ajax-form-button pull-right mb-15',
        'value' => Url::to(['stock/rubber-crumb/receipt/create', 'workShiftId' => $model->id]),
        'data' => ['header' => 'Добавить крошку']
    ]) : ''; ?>

    <h3><b>Произведено:</b></h3>

    <?= \yii\grid\GridView::widget([
        'dataProvider' => (new \yii\data\ActiveDataProvider([
            'query' => $model->getReceipts()->orderBy('date DESC'),
            'sort' => false
        ])),
        'summary' => false,
        'columns' => [
            [
                'attribute' => 'id',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            'date',
            'fractionDescription',
            'quantity:decimal',
            'comment:text',
            [
                'attribute' => 'created',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'attribute' => 'updated',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'class' => 'yii\grid\ActionColumn', 'template' => '{delete}',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::CEO),
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['stock/rubber-crumb/receipt/delete', 'id' => $model->id],
                            [
                                'data' => [
                                    'confirm' => 'Вы уверены?',
                                    'method' => 'post',
                                ],
                            ]);

                    }
                ]
            ],
        ],
    ]) ?>

    <?= Yii::$app->user->can(\app\models\constants\Roles::CEO) ? Html::button('<span class="glyphicon glyphicon-plus-sign" style="font-size: 20px"></span>', [
        'class' => 'btn btn-success ajax-form-button pull-right mb-15',
        'value' => Url::to(['flows/salary/create', 'workShiftId' => $model->id]),
        'data' => ['header' => 'Добавить начисление з/п']
    ]) : ''; ?>
    <h3><b>Начисления з/п:</b></h3>
    <?= \yii\grid\GridView::widget([
        'dataProvider' => (new \yii\data\ActiveDataProvider([
            'query' => $model->getSalaries()->orderBy('updated_at DESC'),
            'sort' => false
        ])),
        'summary' => false,
        'columns' => [
            [
                'attribute' => 'id',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            'employeeFio',
            'salary:currency',
            'additionalWorkName',
            [
                    'attribute' => 'additional_work_salary',
                'value' => function($model) {
                    return $model->additional_work_salary ? Yii::$app->getFormatter()->asCurrency($model->additional_work_salary) : null;
                }
            ],
            'comment:text',
            [
                'attribute' => 'created',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'attribute' => 'updated',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'class' => 'yii\grid\ActionColumn', 'template' => '{delete}',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::CEO),
                'buttons' => [
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                            ['flows/salary/delete', 'id' => $model->id],
                            [
                                'data' => [
                                    'confirm' => 'Вы уверены?',
                                    'method' => 'post',
                                ],
                            ]);

                    }
                ]
            ],
        ],
    ]) ?>
</div>

<?php
Modal::begin([
    'header' => '<h4></h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>
