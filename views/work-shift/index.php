<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\constants\Constant;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\WorkShiftSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Смены (крошка)';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="work-shift-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Добавить смену', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => '№'],

            'date',
            [
                'attribute' => 'workshop',
                'filter' => Constant::workshopsFilter(true),
                'visible' => (Yii::$app->user->identity->getRole() != \app\models\constants\Roles::MASTER),
                'value' => function($model) {
                    return $model->workshopDescription;
                }
            ],
            [
                'attribute' => 'type',
                'filter' => \app\models\constants\Constant::workShifts(true),
                'value' => function ($model) {
                    return $model->typeDescription;
                }
            ],
            'goodsTypeDescription',
            'totalSalarySum:currency',
            [
                'attribute' => 'created',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'attribute' => 'updated',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                            'class' => 'ajax-form-button',
                            'value' => Url::to(['work-shift/update', 'id' => $model->id]),
                            'data' => ['header' => 'Редактировать']
                        ]);

                    },
                ],
                'visibleButtons' => [
                    'update' => Yii::$app->user->can(\app\models\constants\Roles::CEO),
                    'view' => Yii::$app->user->can(\app\models\constants\Roles::CEO),
                    'delete' => Yii::$app->user->can(\app\models\constants\Roles::CEO),
                ]],
        ],
    ]); ?>


</div>
<?php
Modal::begin([
    'header' => '<h4></h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>