<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\WorkShift */

$this->title = 'Редактировать смену № ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Смены (крошка)', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="work-shift-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
