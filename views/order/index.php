<?php

use app\models\Order;
use yii\helpers\Html;
use yii\grid\GridView;
use kartik\select2\Select2;
use app\models\constants\Roles;
use kartik\export\ExportMenu;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
$this->params['breadcrumbs'][] = $this->title;

$columns = [
    ['class' => 'yii\grid\SerialColumn'],
    [
        'attribute' => 'counterparty_id',
        'filter' => Select2::widget([
            'name' => 'OrderSearch[counterparty_id]',
            'model' => $searchModel,
            'data' => \app\models\catalog\Counterparty::ddl(),
            'value' => $searchModel->counterparty_id,
            'options' => ['multiple' => false, 'placeholder' => 'Выберите...'],
            'pluginOptions' => ['allowClear' => true]
        ]),
        'value' => function ($model) {
            return $model->counterpartyName;
        }

    ],
    'shortDescription',
    'quantity:decimal',
    'summary:decimal',
    'comment:ntext',
    [
        'attribute' => 'status',
        'filter' => \app\models\Order::statuses(true),
        'value' => function ($model) {
            return $model->statusDescription;
        }
    ],
    'productionStatusDescription',

    ['class' => 'yii\grid\ActionColumn',
        'visibleButtons' => [
            'delete' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN),
            'update' => in_array(Yii::$app->user->identity->getRole(), [Roles::ADMIN, Roles::CEO, Roles::MARKETER]),
        ]
    ],
];
?>
<div class="order-index">
    <h1><?= Html::encode($this->title) ?></h1>

    <?= ExportMenu::widget([
        'dataProvider' => new \yii\data\ActiveDataProvider([
            'query' => Order::find()->orderBy('updated_at DESC')
        ]),
        'showColumnSelector' => false,
        'filename' => 'Report_by_employees_ ' . date('Y_m_d'),
        'exportConfig' => [
            ExportMenu::FORMAT_TEXT => false,
            ExportMenu::FORMAT_HTML => false,
            ExportMenu::FORMAT_CSV => false,
            ExportMenu::FORMAT_EXCEL => false,
            ExportMenu::FORMAT_PDF => false,
            ExportMenu::FORMAT_EXCEL_X => [
                'label' => 'Сохранить в Excel',
                'iconOptions' => ['class' => 'hidden'],
                'linkOptions' => ['class' => 'btn btn-info'],
                'options' => ['style' => 'list-style-type:none; display:inline-block', 'class' => 'pull-right'],
            ],
        ],
        'asDropdown' => false,
        'showFooter' => true,
        'showPageSummary' => true,
        'columns' => $columns,
    ]); ?>
    <p>
        <?= Html::a('Добавить сделку', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => $columns,
    ]); ?>


</div>
