<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'counterparty_id')->widget(Select2::classname(), [
        'data' => \app\models\catalog\Counterparty::ddl(),
        'options' => ['placeholder' => 'Выберите контрагента...'],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?php
    // $form->field($model, 'goods_type')->dropDownList(\app\models\constants\Constant::workGoodsType(true)) */
    ?>

    <?php if ($model->isNewRecord):?>
    <?= $form->field($model, 'fraction')->dropDownList(\app\models\constants\Constant::rubberCrumbFractions(true)) ?>
    <?php endif;?>

    <?= $form->field($model, 'quantity')->textInput(['number', 'step' => 0.1]) ?>

    <?= $form->field($model, 'summary')->textInput(['number', 'step' => 0.1]) ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
