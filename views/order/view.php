<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\constants\Roles;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = $model->shortDescription;
$this->params['breadcrumbs'][] = ['label' => 'Заказы', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="row">
    <div class="col-md-offset-1">

        <h1><?= Html::encode($this->title) ?></h1>

        <p>
            <?php if (in_array(Yii::$app->user->identity->getRole(), [Roles::ADMIN, Roles::CEO, Roles::MARKETER])): ?>
                <?= Html::a('<span class="glyphicon glyphicon-pencil"></span> Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']); ?>
                <?= Html::button('Изменить статус', [
                    'class' => 'btn btn-primary ajax-form-button',
                    'value' => Url::to(['update-status', 'id' => $model->id]),
                    'data' => ['header' => 'Изменить статус']
                ]); ?>

                <?= Html::a('<span class="glyphicon glyphicon-file"></span> Добавить документ',
                    ['order-file/create', 'orderId' => $model->id],
                    ['class' => 'btn btn-info']); ?>

            <?php endif; ?>

            <?php if (Yii::$app->user->can(\app\models\constants\Roles::ADMIN)) {
                echo Html::a('Удалить', ['delete', 'id' => $model->id], [
                    'class' => 'btn btn-danger',
                    'data' => [
                        'confirm' => 'Вы уверены?',
                        'method' => 'post',
                    ],
                ]);
            }
            ?>
        </p>

        <div class="row">
            <div class="col-md-4">
                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        [
                            'attribute' => 'id',
                            'label' => 'ID',
                            'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
                        ],
                        'counterpartyName',
                        'shortDescription',
                        'quantity:decimal',
                        'summary:decimal',
                        'comment:ntext',
                        'statusDescription',
                        'productionStatusDescription',
                        [
                            'attribute' => 'created',
                            'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
                        ],
                        [
                            'attribute' => 'updated',
                            'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
                        ],
                        [
                            'label' => 'Документы',
                            'format' => 'raw',
                            'value' => function ($model) {
                                $links = '<table class="table">';
                                foreach ($model->files as $file) {
                                    $links .= '<tr>';
                                    $links .= '<td>' . $file->getFileLink() . '</td>' . '<td>' .
                                        Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                            ['order-file/delete', 'id' => $file->id], ['data' => [
                                                'confirm' => 'Вы уверены?',
                                                'method' => 'post',
                                            ],]) . '</td>';
                                    $links .= '</tr>';
                                }

                                $links .= '</table>';
                                return $links;
                            }
                        ]
                    ],
                ]) ?>
            </div>
            <div class="col-md-7">
                <div>
                    <h3 class="pull-left">
                        <b>Поступления</b>
                    </h3>
                    <?php if (in_array(Yii::$app->user->identity->getRole(), [Roles::ADMIN, Roles::CEO, Roles::ACCOUNTANT])): ?>
                        <?= Html::a('<span class="glyphicon glyphicon-plus-sign" style="font-size: 20px"></span>',
                            '#',
                            [
                                'class' => 'btn btn-success pull-right mb-15 ajax-form-button',
                                'value' => Url::to(['flows/rubberCrumb/cash-receipt/create', 'orderId' => $model->id]),
                                'data' => ['header' => 'Добавить поступление средств']
                            ]); ?>

                    <?php endif; ?>
                    <?= GridView::widget([
                        'dataProvider' => (new ActiveDataProvider([
                            'query' => $model->getCashReceipts()->orderBy('date DESC'),
                            'sort' => false,
                            'pagination' => [
                                'pageParam' => 'cash-receipt-page',
                                'pageSizeParam' => 'cash-receipt-per-page',
                                'pageSize' => 5
                            ],
                        ])),
                        'summary' => false,
                        'columns' => [

                            'date',
                            'purposeDescription',
                            'paymentTypeDescription',
                            'recipientDescription',
                            'summary:decimal',
                            'comment:text',
                            ['class' => 'yii\grid\ActionColumn', 'visible' => Yii::$app->user->can(Roles::ADMIN),
                                'buttons' => [
                                    'update' => function ($url, $receiptModel) {

                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                            'class' => 'ajax-form-button',
                                            'value' => Url::to(['flows/rubberCrumb/cash-receipt/update', 'id' => $receiptModel->id]),
                                            'data' => ['header' => 'Редактировать']
                                        ]);

                                    },
                                    'view' => function ($url, $receiptModel) {

                                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                                            'class' => 'ajax-form-button',
                                            'value' => Url::to(['flows/rubberCrumb/cash-receipt/view', 'id' => $receiptModel->id]),
                                            'data' => ['header' => 'Просмотр']
                                        ]);

                                    },
                                    'delete' => function ($url, $receiptModel) {

                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                            Url::to(['flows/rubberCrumb/cash-receipt/delete', 'id' => $receiptModel->id]), [
                                                'value' => Url::to(['flows/rubberCrumb/cash-receipt/delete', 'id' => $receiptModel->id]),
                                                'data' => [
                                                    'confirm' => 'Вы уверены?',
                                                    'method' => 'post',
                                                ]
                                            ]);

                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="row">
                    <p class="pull-right">
                        <span style="font-size: 20px; font-weight: bold">Остаток <?= Yii::$app->formatter->asCurrency($model->debt) ?></span>
                    </p>
                </div>

                <h3><b>Отгружено по заказу</b></h3>

                <div>
                    <?= GridView::widget([
                        'dataProvider' => (new ActiveDataProvider([
                            'query' => $model->getDeductions()->orderBy('date DESC'),
                            'sort' => false,
                            'pagination' => [
                                'pageParam' => 'deductions-page',
                                'pageSizeParam' => 'deductions-per-page',
                                'pageSize' => 5
                            ],
                        ])),
                        'summary' => false,
                        'columns' => [
                            'date',
                            'fractionDescription',
                            'quantity:decimal',
                            'comment:text',
                            ['class' => 'yii\grid\ActionColumn', 'visible' => Yii::$app->user->can(Roles::ADMIN),
                                'buttons' => [
                                    'update' => function ($url, $deductionModel) {

                                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                                            'class' => 'ajax-form-button',
                                            'value' => Url::to(['flows/rubberCrumb/deduction/update', 'id' => $deductionModel->id]),
                                            'data' => ['header' => 'Редактировать']
                                        ]);

                                    },
                                    'view' => function ($url, $deductionModel) {

                                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                                            'class' => 'ajax-form-button',
                                            'value' => Url::to(['flows/rubberCrumb/deduction/view', 'id' => $deductionModel->id]),
                                            'data' => ['header' => 'Просмотр']
                                        ]);

                                    },
                                    'delete' => function ($url, $deductionModel) {

                                        return Html::a('<span class="glyphicon glyphicon-trash"></span>',
                                            Url::to(['flows/rubberCrumb/deduction/delete', 'id' => $deductionModel->id]), [
                                                'value' => Url::to(['flows/rubberCrumb/cash-receipt/delete', 'id' => $deductionModel->id]),
                                                'data' => [
                                                    'confirm' => 'Вы уверены?',
                                                    'method' => 'post',
                                                ]
                                            ]);

                                    },
                                ],
                            ],
                        ],
                    ]); ?>
                </div>
                <div class="row">
                    <p class="pull-right">
                        <span style="font-size: 20px; font-weight: bold">Остаток: <?= Yii::$app->formatter->asDecimal($model->shippingDebt) ?></span>
                    </p>
                </div>
                <div class="row">
                    <p class="pull-right">
                        <span style="font-size: 20px; font-weight: bold">Доступно на складе: <?= \app\helpers\StockHelper::rubberCrumbStock($model->fraction) ?></span>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
Modal::begin([
    'header' => '<h4></h4>',
    'id' => 'modal',
    'size' => 'modal-lg',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>
