<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\constants\Roles;
use app\models\constants\Constant;

/* @var $this yii\web\View */
/* @var $model app\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'role')->dropDownList(Roles::getAssignmentList(true)) ?>

    <?= $form->field($identity, 'login')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'workshop', [
        'options' => [
            'style' => [
                'display' => (!$model->isNewRecord && $model->getRole() === Roles::MASTER) ? 'block' : 'none'
            ]
        ]
    ])->dropDownList(Constant::workshops(true)) ?>


    <?php if ($model->isNewRecord): ?>
        <?= $form->field($identity, 'password')->passwordInput(['maxlength' => true]) ?>

        <?= $form->field($identity, 'checkPassword')->passwordInput(['maxlength' => true]) ?>

    <?php endif; ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>
<?php
$js = "

let workshop = $('.field-user-workshop');
  if($('#user-role').val() == '" . Roles::MASTER . "') {
     workshop.show();
  }
  
  $('#user-role').on('change', function(event) {
     if($(this).val() === '" . Roles::MASTER . "') {
         workshop.slideDown();
     }else {
         workshop.slideUp();
     }
  });
  
";
$this->registerJs($js);
?>
