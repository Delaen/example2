<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\constants\Constant;

/* @var $this yii\web\View */
/* @var $model app\models\catalog\Employee */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="employee-form">

    <?php $form = ActiveForm::begin(['id' => 'modal_form', 'options' => ['enableAjaxValidation' => true]]); ?>

    <?= $form->field($model, 'fio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'workshop')->dropDownList(Constant::workshopsFilter(true))?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
