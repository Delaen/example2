<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\constants\Constant;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\catalog\search\EmployeeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Сотрудники';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="employee-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=Html::button('Добавить сотрудника', [
            'class' => 'btn btn-success ajax-form-button',
            'value' => Url::to(['employee/create']),
            'data' => ['header' => 'Добавить сотрудника']
        ]);?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'fio',
            [
                    'attribute' => 'workshop',
                'filter' => Constant::workshopsFilter(true),
                'value' => function($model) {
                    return $model->workshopDescription;
                }
            ],
            'created',
            'updated',

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                            'class' => 'ajax-form-button',
                            'value' => Url::to(['employee/update', 'id' => $model->id]),
                            'data' => ['header' => 'Редактировать']
                        ]);

                    },
                    'view' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                            'class' => 'ajax-form-button',
                            'value' => Url::to(['employee/view', 'id' => $model->id]),
                            'data' => ['header' => 'Просмотр: ' . $model->fio]
                        ]);

                    }
                ]
            ],
        ],
    ]); ?>

    <?php
    Modal::begin([
        'header' => '<h4></h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>
</div>
