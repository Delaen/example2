<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Electricity */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Электричество', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="electricity-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'date',
            'reading_meter',
            'comment:ntext',
            [
                'attribute' => 'created',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
            [
                'attribute' => 'updated',
                'visible' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN)
            ],
        ],
    ]) ?>

</div>
