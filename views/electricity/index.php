<?php

use yii\helpers\Html;
use yii\grid\GridView;
use app\models\constants\Constant;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\search\ElectricitySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Электричество';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="electricity-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?=Html::button('Добавить показания', [
            'class' => 'btn btn-success ajax-form-button',
            'value' => Url::to(['electricity/create']),
            'data' => ['header' => 'Добавить показания']
        ]);?>
    </p>

    <p>
    <?php if (Yii::$app->user->identity->getRole() != \app\models\constants\Roles::MASTER):?>
        <?= Html::dropDownList('Electricity[workshop]', $searchModel->workshop, Constant::workshopsFilter(true), [
            'class' => 'btn btn-light',
            'id' => 'workshop-ddl',
            'style' => ['border-color' => '#aaaaaa']
        ]); ?>
    <?php endif;?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
        'summary' => false,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn', 'header' => '№'],

           // 'id',
            'date',
            'reading_meter',
            'used',
            'comment:ntext',

            ['class' => 'yii\grid\ActionColumn',
                'buttons' => [
                    'update' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', '#', [
                            'class' => 'ajax-form-button',
                            'value' => Url::to(['electricity/update', 'id' => $model->id]),
                            'data' => ['header' => 'Редактировать']
                        ]);

                    },
                    'view' => function ($url, $model) {

                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', '#', [
                            'class' => 'ajax-form-button',
                            'value' => Url::to(['electricity/view', 'id' => $model->id]),
                            'data' => ['header' => 'Просмотр']
                        ]);

                    }
                ],
                'visibleButtons' => [
                        'update' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN),
                        'view' => Yii::$app->user->can(\app\models\constants\Roles::ADMIN),
                        'delete' => Yii::$app->user->can(\app\models\constants\Roles::CEO),
                ]
            ],
        ],
    ]); ?>

    <?php
    Modal::begin([
        'header' => '<h4></h4>',
        'id' => 'modal',
        'size' => 'modal-lg',
    ]);
    echo "<div id='modalContent'></div>";
    Modal::end();
    ?>

</div>

<?php
$js = "
   $('#workshop-ddl').on('change', function(idx, el) {
        window.location.replace('" . Url::to(['']) . "' + '?ElectricitySearch%5Bworkshop%5D=' + $(this).val());
   });
";

$this->registerJs($js);
?>