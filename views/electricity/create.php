<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Electricity */

$this->title = 'Добавить расход';
$this->params['breadcrumbs'][] = ['label' => 'Электричество', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="electricity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
