<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\catalog\Counterparty */

$this->title = 'Добавить контрагента';
$this->params['breadcrumbs'][] = ['label' => 'Контрагенты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="counterparty-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
