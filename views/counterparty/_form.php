<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\constants\Roles;
use app\models\User;

/* @var $this yii\web\View */
/* @var $model app\models\catalog\Counterparty */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="counterparty-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'contact_person')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?php if (Yii::$app->user->can(Roles::ADMIN)): ?>
        <?= $form->field($model, 'creator_id')->dropDownList(User::ddlMarketers()) ?>
    <?php endif; ?>

    <?= $form->field($model, 'comment')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
