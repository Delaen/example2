<?php

use yii\bootstrap\NavBar;
use yii\bootstrap\Nav;
use yii\helpers\Html;
use app\models\constants\Constant;
use app\models\constants\Roles;

?>

<?php
NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],
]); ?>

<?php
$navItems = [];


if (!Yii::$app->user->isGuest) {
    $role = Yii::$app->user->identity->getRole();
    if (Yii::$app->user->can(Roles::CEO)) {
        $navItems = [
            [
                'label' => 'Заказы',
                'url' => ['order/index']
            ],
            [
                'label' => 'Смены',
                'url' => ['work-shift/index']
            ],
            [
                'label' => 'Склад',
                'url' => ['stock/rubber-crumb/index']
            ],
            [
                'label' => 'Доп.расходы',
                'url' => ['cost/index']
            ],
            [
                'label' => 'Электричество',
                'url' => ['electricity/index']
            ],
            [
                'label' => 'Отчеты',
                'items' => [
                    ['label' => 'Общий', 'url' => ['report/main']],
                    ['label' => 'По сотрудникам', 'url' => ['report/by-employees']],
                ]
            ],
            [
                'label' => 'Справочник',
                'items' => [
                    ['label' => 'Типовые работы', 'url' => ['model-work/index']],
                    ['label' => 'Расходы', 'url' => ['cost-catalog/index']],
                    ['label' => 'Контрагенты', 'url' => ['counterparty/index']],
                    ['label' => 'Пользователи', 'url' => ['user/index']],
                    ['label' => 'Сотрудники', 'url' => ['employee/index']],
                ]
            ],

        ];
    }

    switch (Yii::$app->user->identity->getRole()) {
        case Roles::MASTER:
            $navItems = [
                [
                    'label' => 'Смены',
                    'url' => ['work-shift/index']
                ],
                [
                    'label' => 'Склад',
                    'url' => ['stock/rubber-crumb/index']
                ],
                [
                    'label' => 'Отчет',
                    'url' => ['report/main']
                ],
                [
                    'label' => 'Типовые работы',
                    'url' => ['model-work/index']
                ],
            ];
            break;
        case Roles::MARKETER:
            $navItems = [
                [
                    'label' => 'Заказы',
                    'url' => ['order/index']
                ],
                [
                    'label' => 'Контрагенты',
                    'url' => ['counterparty/index']
                ],
            ];
            break;
        case Roles::ACCOUNTANT:
            $navItems = [
                [
                    'label' => 'Заказы',
                    'url' => ['order/index']
                ],
            ];
            break;
    }
}
?>

<?php
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => array_merge(
        $navItems,
        [Yii::$app->user->isGuest ? (
        ['label' => 'Вход', 'url' => ['/site/login']]
        ) : (
            '<li>'
            . Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Выход (' . Yii::$app->user->identity->login . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>'
        )]
    ),
]);
NavBar::end();
?>