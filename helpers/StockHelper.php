<?php

namespace app\helpers;

use app\models\constants\Constant;
use app\models\flows\rubberCrumb\Receipt;
use app\models\flows\rubberCrumb\Receipt as RubberCrumbReceipt;
use app\models\flows\rubberCrumb\Deduction as RubberCrumbDeduction;
use yii\base\InvalidConfigException;
use yii\db\Expression;
use yii\db\Query;
use yii\validators\DateValidator;
use yii\web\HttpException;

/**
 * Class StockHelper
 * @package app\helpers
 */
abstract class StockHelper
{
    /**
     * Возвращает остаток на складе по определенной фракции
     * При указании цеха и даты выдает результат по опр. цеху и на опр.дату соответственно
     * @param string $fraction
     * @param string|null $workshop
     * @param string|null $date
     * @param string $dateOperator
     * @return float
     * @throws InvalidConfigException
     */
    static function rubberCrumbStock(string $fraction, string $workshop = null, $date = null, string $dateOperator = '<='): float
    {
        if (!in_array($fraction, Constant::rubberCrumbFractions())) {
            throw new InvalidConfigException('Неверное значение для типа фракции');
        }

        if ($workshop && !in_array($workshop, Constant::workshops())) {
            throw new InvalidConfigException('Неверное значение для типа цеха');
        }

        $receiptQuery = RubberCrumbReceipt::find()->select(['sum' => 'sum(quantity)'])
            ->where(['fraction' => $fraction]);

        $deductionQuery = RubberCrumbDeduction::find()->select(['sum' => 'sum(quantity) * (-1)'])
            ->where(['fraction' => $fraction]);

        if ($workshop) {
            $receiptQuery->andWhere(['workshop' => $workshop]);
            $deductionQuery->andWhere(['workshop' => $workshop]);
        }

        $validator = new DateValidator(['format' => 'php:Y-m-d']);
        $valid = $validator->validate($date);
        $validator->format = 'php:d.m.Y';
        $valid = $valid || $validator->validate($date);

        if ($date && $valid && in_array($dateOperator, ['>=', '<=', '>', '<', '=', '!='])) {
            $date = date('Y-m-d', strtotime($date));
            $receiptQuery->andWhere([$dateOperator, 'date', $date]);
            $deductionQuery->andWhere([$dateOperator, 'date', $date]);
        }

        $sum = (new Query())->select(['sum' => 'sum(sum)'])
            ->from('(' . $receiptQuery->union($deductionQuery->createCommand()->rawSql)->createCommand()->rawSql . ') as sumTable')
            ->column();

        return (int)(empty($sum) ? 0 : $sum[0]);
    }

    /**
     * Возвращает таблицу отражающие остатки по всем фракциям указанного цеха
     * @param string $workshop
     * @return string
     * @throws InvalidConfigException
     */
    static function rubberCrumbStockHtml(string $workshop)
    {
        if (!in_array($workshop, Constant::workshopsFilter())) {
            throw new InvalidConfigException(400, 'Неверно указан цех');
        }

        $html = '<table class="table table-bordered table-striped" style="font-size: 18px">';
        $html .= '<tr> <th colspan="2">' . Constant::workshops(true)[$workshop] . '</th></tr>';

        foreach (Constant::rubberCrumbFractions(true) as $fraction => $description) {
            $html .= '<tr>';
            $html .= "<td>$description</td>";
            $html .= '<td>' . StockHelper::rubberCrumbStock($fraction, $workshop) . ' кг.</td>';

        }
        $html .= '</table>';

        return $html;
    }

    /**
     * Валидируются потоки начиная с первого до последнего (за исключением удаляемого) путем сложения нынешней суммы со следующим значением.
     * Если в процессе првоерки появляется минусовая сумма, функция выкидывает false
     * @param RubberCrumbReceipt $receipt
     * @return bool
     */
    static function validateDeletingRubberCrumbReceipt(Receipt $receipt): bool
    {
        $receiptQuery = RubberCrumbReceipt::find()
            ->select('IF(`id`='.$receipt->id.', [[quantity]]*0 , `quantity`) as quantity, `id`, `date`')
            ->where(['fraction' => $receipt->fraction])
            ->andWhere(['workshop' => $receipt->workshop]);

        $deductionQuery = RubberCrumbDeduction::find()->select(['quantity * (-1)', 'id', 'date'])
            ->where(['fraction' => $receipt->fraction])
            ->andWhere(['workshop' => $receipt->workshop]);

        $flows = (new Query())->select(['flows.quantity'])
            ->from('(' . $receiptQuery->union($deductionQuery->createCommand()->rawSql)->createCommand()->rawSql . ') as flows')
            ->orderBy('date ASC, id DESC')->column();

        $sum = 0;
        foreach ($flows as $flow) {
            $sum += $flow;
            if ($sum < 0) {
                return false;
            }
        }

        return true;
    }
}