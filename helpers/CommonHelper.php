<?php

namespace app\helpers;

use Yii;

class CommonHelper
{
    /**
     * @return string
     * @throws \Exception
     */
    static function generateIdx()
    {
        $bytes = random_bytes(16);
        $bytes[6] = chr((ord($bytes[6]) & 0x0f) | 0x40);
        $bytes[8] = chr((ord($bytes[8]) & 0x3f) | 0x80);
        $hex = bin2hex($bytes);

        $fields = [
            'time_low' => substr($hex, 0, 8),
            'time_mid' => substr($hex, 8, 4),
            'time_hi_and_version' => substr($hex, 12, 4),
            'clock_seq_hi_and_reserved' => substr($hex, 16, 2),
            'clock_seq_low' => substr($hex, 18, 2),
            'node' => substr($hex, 20, 12)
        ];

        return vsprintf(
            '%08s-%04s-%04s-%02s%02s-%012s',
            $fields
        );
    }

    /**
     * @param $str_length
     * @return bool|string
     * @throws \Exception
     */
    static function randomString($str_length)
    {
        $str_characters = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');

        if (!is_int($str_length) || $str_length < 0) {
            return false;
        }

        $characters_length = count($str_characters) - 1;

        $string = '';

        for ($i = $str_length; $i > 0; $i--) {
            $string .= $str_characters[random_int(0, $characters_length)];
        }

        return $string;
    }

    /**
     * @param $file
     */
    static function deleteFile($file)
    {
        if ($file) {
            file_exists($file) && !is_dir($file) && unlink($file);
        }
    }
    //Проверяет наличие ключей в массиве $search, перечисленных в $needle.
    //Если массив $search не передан, читаются параметры get, либо параметры переданные в теле запроса
    static function checkParamsExistence(array $needle, array $search = [])
    {
        $params = !empty($search) ? $search : (Yii::$app->request->isGet ? Yii::$app->request->getQueryParams() : Yii::$app->request->bodyParams);
        if ($params) {
            if (count($needle) === count(array_intersect($needle, array_keys($params)))) {
                return true;
            }
        }
        return false;
    }

    static function jsonDecode($str)
    {
        $result = json_decode($str, true);
        return json_last_error() === JSON_ERROR_NONE ? $result : false;
    }

    static function getUrlWithParam($param, $value)
    {

        $params = Yii::$app->request->queryParams;
        $params[$param] = $value;

        $queryString = '';

        foreach ($params as $key => $value) {
            $queryString .= "&$key=$value";
        }

        $queryString && $queryString[0] = '?';

        return Yii::$app->requestedRoute . $queryString;
    }
}