Структура проекта
-------------------

      assets/               пакеты ресурсов  
      components/           содержит трэйты и поведения, реализующие часто используемую логику (загрузка, рэндер изображений, рэндер аттрибутов и т.д.)
      config/               конфигурация
      console/controllers/  консольные контроллеры
      console/migrations/   миграции
      controllers/          контроллеры
      helpers/              хэлперы  
      mail/                 view-файлы для формирования писем
      migrtions/            миграции
      models/               модели
      models/catalog        модели раздела "Справочник"
      runtime/              contains files generated during runtime
      vendor/               contains dependent 3rd-party packages
      views/                contains view files for the Web application
      web/                  contains the entry script and Web resources
      widgets/              содержит виджеты. Помимо стандартного Alert здесь находятся виджеты кастомизации yii\grid\Column



Требования
------------
Минимально требуемая версия PHP - 7.2.0

**Установка**
------------
- Скачать проект
~~~
git clone <project-path>
~~~
- Заменить все файлы /config/*.example на соответствующие конфиги
- Создать базу данных, указать настройки подключения в /config/db.php
- Подтянуть все зависимости 
~~~
composer install
~~~
- Применить миграции
~~~
yii migrate
~~~
- Инициализировать Rbac-роли
~~~
yii rbac/init
~~~

Примечания
------------
Для получаения информации об остатках крошки используется app\helpers\StockHelper. 
C его помощью можно узнать количество остатков крошки опр. фракции в целом и с разбиением по цеху и дате.
Также с помощью хэлпера можно вызвать рэндер таблиц остатков по цехам и валидацию удаления записи

