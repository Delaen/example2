<?php
namespace app\console\controllers;

use app\models\constants\Roles;
use yii\console\Controller;
use Yii;
use yii\helpers\Console;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $admin = $auth->createRole(Roles::ADMIN);
        $ceo = $auth->createRole(Roles::CEO);
        $master = $auth->createRole(Roles::MASTER);
        $accountant = $auth->createRole(Roles::ACCOUNTANT);
        $marketer = $auth->createRole(Roles::MARKETER);

        $auth->add($admin);
        $auth->add($ceo);
        $auth->add($master);
        $auth->add($accountant);
        $auth->add($marketer);

        $auth->addChild($ceo, $master);
        $auth->addChild($ceo, $accountant);
        $auth->addChild($ceo, $marketer);

        $auth->addChild($admin, $ceo);

        $auth->assign($admin, '7fs43873-c18d-4972-3580-a0983a5c6f50');

      Console::stdout(Console::ansiFormat("Roles initiated" . PHP_EOL, [Console::FG_GREEN]));
    }
}