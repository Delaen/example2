<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%counterparty}}`.
 */
class m200109_045231_create_counterparty_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%counterparty}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'contact_person' => $this->string(),
            'phone_number' => $this->string(),
            'email' => $this->string(),
            'creator_id' => $this->integer(),
            'comment' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (creator_id) REFERENCES user(id) ON UPDATE CASCADE ON DELETE RESTRICT'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%counterparty}}');
    }
}
