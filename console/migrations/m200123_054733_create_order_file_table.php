<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%order_file}}`.
 */
class m200123_054733_create_order_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%order_file}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
            'file' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (order_id) REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE',
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%order_file}}');
    }
}
