<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%costs_catalog}}`.
 */
class m200109_045008_create_costs_catalog_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%costs_catalog}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'unit_of_measurement' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%costs_catalog}}');
    }
}
