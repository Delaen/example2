<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rubber_crumb_cash_receipts}}`.
 */
class m200114_092118_create_rubber_crumb_cash_receipts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rubber_crumb_cash_receipts}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'date' => $this->date()->notNull(),
            'purpose' => $this->string(30)->notNull(),
            'recipient' => $this->string(30)->notNull(),
            'payment_type' => $this->string(30)->notNull(),
            'summary' => $this->float()->notNull(),
            'comment' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (order_id) REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rubber_crumb_cash_receipts}}');
    }
}
