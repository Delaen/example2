<?php

use yii\db\Migration;

/**
 * Class m200109_031710_create_rbac_tables
 */
class m200109_031710_create_rbac_tables extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        //Запускаем миграцию для создания rbac-таблиц (yii migrate --migrationPath=@yii/rbac/migrations)
        (new \yii\console\controllers\MigrateController('migrate', Yii::$app))
            ->runAction('up', ['migrationPath' => '@yii/rbac/migrations/', 'interactive' => false]);
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        \yii\helpers\Console::stdout('Не получилось');
        die;
    }
}
