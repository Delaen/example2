<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%work_shift}}`.
 */
class m200109_045923_create_work_shift_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%work_shift}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull(),
            'type' => $this->string()->notNull(),
            'goods_type' => $this->string()->notNull(),
            'workshop' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%work_shift}}');
    }
}
