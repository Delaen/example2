<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%costs}}`.
 */
class m200109_045557_create_costs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%costs}}', [
            'id' => $this->primaryKey(),
            'date' => $this->date()->notNull(),
            'cost_catalog_id' => $this->integer(),
            'workshop' => $this->string()->notNull(),
            'quantity' => $this->float()->defaultValue(0.0),
            'comment' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (cost_catalog_id) REFERENCES costs_catalog(id) ON UPDATE CASCADE ON DELETE RESTRICT'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%costs}}');
    }
}
