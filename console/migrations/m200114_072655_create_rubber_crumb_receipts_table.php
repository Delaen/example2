<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rubber_crumb_receipts}}`.
 */
class m200114_072655_create_rubber_crumb_receipts_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rubber_crumb_receipts}}', [
            'id' => $this->primaryKey(),
            'work_shift_id' => $this->integer(),
            'date' => $this->date(),
            'workshop' => $this->string()->notNull(),
            'fraction' => $this->string(30),
            'quantity' => $this->float(),
            'comment' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (work_shift_id) REFERENCES work_shift(id) ON UPDATE CASCADE ON DELETE CASCADE'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rubber_crumb_receipts}}');
    }
}
