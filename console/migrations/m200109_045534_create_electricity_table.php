<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%electricity}}`.
 */
class m200109_045534_create_electricity_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%electricity}}', [
            'id' => $this->primaryKey(),
            'workshop' => $this->string()->notNull(),
            'date' => $this->date()->notNull(),
            'reading_meter' => $this->integer(),
            'comment' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%electricity}}');
    }
}
