<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rubber_crumb_deduction}}`.
 */
class m200114_072707_create_rubber_crumb_deduction_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rubber_crumb_deduction}}', [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
            'workshop' => $this->string()->notNull(),
            'date' => $this->date(),
            'fraction' => $this->string(30),
            'quantity' => $this->float(),
            'comment' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (order_id) REFERENCES orders(id) ON UPDATE CASCADE ON DELETE CASCADE'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rubber_crumb_deduction}}');
    }
}
