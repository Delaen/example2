<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rubber_crumb_salary}}`.
 */
class m200114_095335_create_rubber_crumb_salary_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rubber_crumb_salary}}', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer(),
            'work_shift_id' => $this->integer(),
            'salary' => $this->float(),
            'additional_work_id' => $this->integer(),
            'additional_work_salary' => $this->float(),
            'comment' => $this->text(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'FOREIGN KEY (employee_id) REFERENCES employee(id) ON UPDATE CASCADE ON DELETE RESTRICT',
            'FOREIGN KEY (additional_work_id) REFERENCES model_work(id) ON UPDATE CASCADE ON DELETE RESTRICT',
            'FOREIGN KEY (work_shift_id) REFERENCES work_shift(id) ON UPDATE CASCADE ON DELETE CASCADE'
        ], 'CHARACTER SET utf8 COLLATE utf8_general_ci');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rubber_crumb_salary}}');
    }
}
