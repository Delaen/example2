<?php

namespace app\controllers;

use app\models\catalog\ModelWork;
use app\models\constants\Roles;
use app\models\forms\rubberCrumb\WorkShiftForm;
use Yii;
use app\models\WorkShift;
use app\models\search\WorkShiftSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * WorkShiftController implements the CRUD actions for WorkShift model.
 */
class WorkShiftController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Roles::ADMIN]
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index', 'view', 'create', 'update'],
                        'roles' => [Roles::MASTER],
                    ],
                ],
            ],
        ];
    }

    public function afterAction($action, $result)
    {
        if ($result === 'success' && in_array($action->id, ['update'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Lists all WorkShift models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new WorkShiftSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single WorkShift model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new WorkShift model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new WorkShiftForm();
        $additionalWorks = ModelWork::ddlWithNoSelect();

        if (Yii::$app->user->identity->getRole() == Roles::MASTER) {
            $model->workshop = Yii::$app->user->identity->user->workshop;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $id = $model->save();
            if (is_int($id)) {
                return $this->redirect(['view', 'id' => $id]);
            }
        }

        return $this->render('create', compact('model', 'additionalWorks'));
    }

    /**
     * Updates an existing WorkShift model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Изменения сохранены');
            return 'success';
        }

        return $this->renderAjax('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing WorkShift model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the WorkShift model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return WorkShift the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = WorkShift::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
