<?php

namespace app\controllers;

use app\models\constants\Roles;
use app\models\Identity;
use Yii;
use app\models\User;
use app\models\search\UserSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Roles::CEO]
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $identity = new Identity();
        $model = new User(['identity' => $identity->id]);

        $transaction = Yii::$app->db->beginTransaction();
        if ($model->load(Yii::$app->request->post()) && $identity->load(Yii::$app->request->post()) &&
            $identity->save() && $model->save()) {

            Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $identity->id);
            $transaction->commit();
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', compact('model', 'identity'));
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->role = $model->getRole();
        $identity = $model->entity;

        if ($model->load(Yii::$app->request->post()) && $model->entity->load(Yii::$app->request->post()) &&
            $model->entity->save() && $model->save()) {

            Yii::$app->authManager->revokeAll($model->entity->id);
            Yii::$app->authManager->assign(Yii::$app->authManager->getRole($model->role), $model->entity->id);
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', compact('model', 'identity'));
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        $transaction = Yii::$app->db->beginTransaction();

        try {
            Yii::$app->authManager->revokeAll($model->entity->id);
            $model->entity->delete();
        }catch (\Exception $e) {
            throw new HttpException(400, 'Нельзя удалить пользователя со связанными данными');
        }

        $transaction->commit();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
