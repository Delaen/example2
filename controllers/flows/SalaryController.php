<?php

namespace app\controllers\flows;

use app\models\constants\Roles;
use app\models\flows\rubberCrumb\Receipt;
use app\models\flows\rubberCrumb\Salary;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * SalaryController implements the CRUD actions for Deduction model.
 */
class SalaryController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Roles::CEO]
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getViewPath()
    {
        return '@app/views/flows/salary';
    }

    public function afterAction($action, $result)
    {
        if ($result === 'success' && in_array($action->id, ['create'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Увеличиваем складской остаток
     * @param null $workShiftId
     * @return string
     */
    public function actionCreate($workShiftId = null)
    {
        $model = new Salary(['work_shift_id' => $workShiftId]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Запись добавлена');
            return 'success';
        }

        return $this->renderAjax('_form_work_shift_view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Deduction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Salary the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Salary::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
