<?php

namespace app\controllers\flows\rubberCrumb;

use app\models\constants\Roles;
use app\models\forms\rubberCrumb\DeductionForm;
use app\models\flows\rubberCrumb\Deduction;
use app\models\Order;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use Yii;

/**
 * DeductionController implements the CRUD actions for Deduction model.
 */
class DeductionController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Roles::ADMIN]
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [Roles::MASTER],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getViewPath()
    {
        return '@app/views/flows/rubber-crumb/deduction';
    }

    public function afterAction($action, $result)
    {
        if ($result === 'success' && in_array($action->id, ['create', 'update', 'view'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Displays a single Deduction model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Уменьшаем складские остаток
     * @return string
     */
    public function actionCreate()
    {
        $model = new DeductionForm();
        $orders = ArrayHelper::map(Order::findByShipping()->all(), 'id', function($model) {
            return "Заказ №$model->id ($model->shortDescription)";
        });

        if (Yii::$app->user->identity->getRole() == Roles::MASTER) {
            $model->workshop = Yii::$app->user->identity->user->workshop;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() &&  $model->saveFractions()) {
            Yii::$app->session->addFlash('success', 'Отгрузка добавлена');
            return 'success';
        }

        if ($model->hasErrors()) {
            Yii::$app->session->addFlash('error', $model->errors[key($model->errors)][0]);
            return 'success';
        }

        return $this->renderAjax('_form_stock', compact('model', 'orders'));
    }

    /**
     * Updates an existing Deduction model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Изменения сохранены');
            return 'success';
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Deduction model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Deduction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Deduction the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Deduction::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
