<?php

namespace app\controllers\flows\rubberCrumb;

use app\models\constants\Roles;
use app\models\flows\rubberCrumb\search\StockSearch;
use app\models\forms\rubberCrumb\ReceiptForm;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * CashReceiptController implements the CRUD actions for CashReceipt model.
 */
class StockController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Roles::ADMIN]
                    ],
                    [
                        'allow' => true,
                        'actions' => ['index'],
                        'roles' => [Roles::MASTER],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getViewPath()
    {
        return '@app/views/flows/rubber-crumb';
    }

    public function afterAction($action, $result)
    {
        if ($result === 'success' && in_array($action->id, ['update', 'view'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Lists all flow items of rubber crumb.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new StockSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CashReceipt model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }
}
