<?php

namespace app\controllers\flows\rubberCrumb;

use app\helpers\StockHelper;
use app\models\constants\Roles;
use app\models\flows\rubberCrumb\Receipt;
use app\models\forms\rubberCrumb\ReceiptForm;
use app\models\WorkShift;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * DeductionController implements the CRUD actions for Deduction model.
 */
class ReceiptController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Roles::ADMIN]
                    ],
                    [
                        'allow' => true,
                        'actions' => ['delete'],
                        'roles' => [Roles::CEO]
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create'],
                        'roles' => [Roles::MASTER],
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getViewPath()
    {
        return '@app/views/flows/rubber-crumb/receipt';
    }

    public function afterAction($action, $result)
    {
        if ($result === 'success' && in_array($action->id, ['create'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Увеличиваем складской остаток
     * @param null $workShiftId
     * @return string
     * @throws HttpException
     */
    public function actionCreate($workShiftId = null)
    {
        $model = new ReceiptForm();
        if ($workShiftId) {
            $workShift = WorkShift::findOne($workShiftId);
            if (!$workShiftId) {
                throw new HttpException(404, 'Смена не найдена');
            }
            $model->date = $workShift->date;
            $model->workshop = $workShift->workshop;
        }

        if (Yii::$app->user->identity->getRole() == Roles::MASTER) {
            $model->workshop = Yii::$app->user->identity->user->workshop;
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $model->saveFractions($workShiftId);

            Yii::$app->session->addFlash('success', 'Крошка добавлена');
            return 'success';
        }

        return $this->renderAjax('_form_stock', [
            'model' => $model,
            'workShiftId' => $workShiftId,
        ]);
    }

    public function actionDelete($id)
    {
        $model = $this->findModel($id);

        if (StockHelper::validateDeletingRubberCrumbReceipt($model)) {
            $model->delete();
            Yii::$app->session->addFlash('success', 'Удалено');
        }else {
            Yii::$app->session->addFlash('error', 'При удалении возникает минусовой остаток за период');
        }

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the Deduction model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Receipt the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Receipt::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
