<?php

namespace app\controllers;

use app\helpers\CommonHelper;
use app\models\constants\Roles;
use Yii;
use app\models\OrderFile;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * OrderFileController implements the CRUD actions for OrderFile model.
 */
class OrderFileController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Roles::MARKETER]
                    ],
                ],
            ],
        ];
    }

    /**
     * @return string
     */
    public function getViewPath()
    {
        return '@app/views/order/files';
    }

    /**
     * Creates a new OrderFile model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param int $orderId
     * @return string
     */
    public function actionCreate(int $orderId)
    {
        $model = new OrderFile(['order_id' => $orderId]);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['order/view', 'id' => $orderId]);
        }

        return $this->render('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing OrderFile model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        CommonHelper::deleteFile($model->defaultPath . $model->file);
        $model->delete();

        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Finds the OrderFile model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return OrderFile the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = OrderFile::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
