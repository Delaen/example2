<?php

namespace app\controllers;

use app\models\constants\Roles;
use app\models\reports\ReportByEmployees;
use app\models\reports\ReportMain;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class ReportController extends Controller
{

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Roles::CEO]
                    ],
                    [
                        'allow' => true,
                        'actions' => ['main'],
                        'roles' => [Roles::MASTER],
                    ],
                ],
            ],
        ];
    }

    public function actionMain()
    {
        $searchModel = new ReportMain();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('main', compact('searchModel', 'dataProvider'));
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionByEmployees()
    {
        $searchModel = new ReportByEmployees();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('by-employees', compact('searchModel', 'dataProvider'));
    }


}
