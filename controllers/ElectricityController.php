<?php

namespace app\controllers;

use app\models\catalog\Employee;
use app\models\constants\Constant;
use app\models\constants\Roles;
use Yii;
use app\models\Electricity;
use app\models\search\ElectricitySearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;

/**
 * ElectricityController implements the CRUD actions for Electricity model.
 */
class ElectricityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => [Roles::CEO],
                    ],
                ],
            ],
        ];
    }

    public function afterAction($action, $result)
    {
        if ($result === 'success' && in_array($action->id, ['create', 'update'])) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        return parent::afterAction($action, $result);
    }

    /**
     * Lists all Electricity models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ElectricitySearch();
        if (Yii::$app->user->identity->getRole() == Roles::MASTER) {
            $searchModel->workshop = Yii::$app->user->identity->user->workshop;
        }

        !$searchModel->workshop && $searchModel->workshop = key(Constant::workshopsFilter(true));
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Electricity model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Electricity model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Electricity();
        if (Yii::$app->user->identity->getRole() == Roles::MASTER) {
            $model->workshop = Yii::$app->user->identity->user->workshop;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Показания сохранены');
            return 'success';
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Electricity model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', 'Изменения сохранены');
            return 'success';
        }

        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Electricity model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Electricity model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Electricity the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Electricity::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
