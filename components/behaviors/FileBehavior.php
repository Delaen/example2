<?php

namespace app\components\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\ErrorException;
use yii\db\ActiveRecord;
use yii\web\UploadedFile;
use yii\imagine\Image;
use app\components\ImageResizer;
use app\components\ImageRotator;

/**
 * Class FileBehavior
 * @package app\components\behaviors\file
 */
class FileBehavior extends Behavior
{
    /**
     * @var string
     */
    public $fileAttrName;

    /**
     * @var int
     */
    public $deleteFileAttrName;

    /**
     * @var string
     */
    public $dir;

    /**
     * @var string
     */
    private $prevFileName;

    /**
     * @var int
     */
    public $width = null;

    /**
     * @var int
     */
    public $height = null;

    /**
     * @var mixed
     */
    public $watermark = false;
    /**
     * @var string
     */
    public $thumbDir;
    /**
     * @var null | int
     */
    public $thumbWidth = null;
    /**
     * @var null | int
     */
    public $thumbHeight = null;
    /**
     * @var null | int
     */
    public $rotation = null;

    /**
     * @return ImageResizer
     */
    public function getResizer()
    {
        return new ImageResizer();
    }

    /**
     * @return ImageRotator
     */
    public function getRotator()
    {
        return new ImageRotator();
    }

    /**
     * @return array
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeInsert',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeUpdate',
            ActiveRecord::EVENT_BEFORE_DELETE => 'beforeDelete',
        ];
    }

    /**
     * @return bool
     */
    public function beforeInsert()
    {
        $this->upload();
    }

    /**
     * @return bool
     */
    public function beforeUpdate()
    {
        $this->prevFileName = $this->owner->getOldAttribute($this->fileAttrName);

        if ($this->owner->{$this->deleteFileAttrName}) {
            $this->deletePrev();
        }

        $this->upload();
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        $this->unlinkFile($this->thumbDir, $this->owner->{$this->fileAttrName});
        return $this->unlinkFile($this->dir, $this->owner->{$this->fileAttrName});
    }

    /**
     * @return bool
     * @throws ErrorException
     */
    private function upload()
    {
        $modelFileAttr = $this->owner->{$this->fileAttrName};

        if ($modelFileAttr instanceof UploadedFile) {
            $file = $modelFileAttr;
        } else {
            $file = UploadedFile::getInstance($this->owner, $this->fileAttrName);
        }

        if ($file) {
            if (!$this->owner->isNewRecord && $this->prevFileName != null) {
                $this->deletePrev();
            }

            $fileName = time() . '_' . md5(Yii::$app->security->generateRandomString(20)) . '.' . strtolower($file->extension);

            !file_exists($this->dir) && mkdir($this->dir, '0777', true);

            if ($file->saveAs($this->dir . $fileName)) {
                $this->owner->{$this->fileAttrName} = $fileName;

                if ($this->rotation !== null) {
                    $this->rotate($this->dir . $fileName);
                }

                if ($this->width != null || $this->height != null) {
                    $this->resize($this->dir . $fileName);
                }

                if ($this->thumbWidth != null || $this->thumbHeight != null) {
                    $this->createThumbnail($this->dir, $fileName);
                }

                if ($this->watermark !== false) {
                    $this->getWatermarkAdder()->addWatermark($this->dir, $fileName, $file->extension, $this->watermark);
                }
            }
        } elseif (!$this->owner->isNewRecord && !$this->owner->{$this->deleteFileAttrName}) {
            $this->owner->{$this->fileAttrName} = $this->prevFileName;
        }
    }

    /**
     * @param $file
     */
    private function resize($file)
    {
        $resizer = $this->getResizer();
        $resizer->setFile($file);

        if ($this->width != null) {
            $resizer->resizeToWidth($this->width);
        }

        if ($this->height != null) {
            $resizer->resizeToHeight($this->height);
        }

        $resizer->save($file);
    }

    /**
     * @param $file
     */
    private function rotate($file)
    {
        $rotator = $this->getRotator();
        $rotator->setFile($file);
        $rotator->rotate($this->rotation);

        $rotator->save($file);
    }

    private function createThumbnail($dir, $fileName)
    {
        !file_exists($this->thumbDir) && mkdir($this->thumbDir);
        Image::thumbnail($dir . $fileName, $this->thumbWidth, $this->thumbHeight)
            ->save(Yii::getAlias($this->thumbDir . $fileName));
    }

    /**
     * @return bool
     */
    private function deletePrev()
    {
        $this->unlinkFile($this->dir, $this->prevFileName);
        $this->unlinkFile($this->thumbDir, $this->prevFileName);
        $this->owner->{$this->fileAttrName} = null;
    }

    /**
     * @param $directory
     * @param $fileName
     * @return bool
     */
    private function unlinkFile($directory, $fileName)
    {
        if ($fileName != null && file_exists($directory . $fileName)) {
            unlink($directory . $fileName);
        }
    }

}

?>