<?php
namespace app\components;

/**
 * Class ImageResizer
 * @package app\components\behaviors\file
 */
class ImageRotator
{
    /**
     * @var resource
     */
    public $image;
    /**
     * @var string
     */
    public $fileName;
    /**
     * @var string
     */
    public $imageType;
    /**
     * @param $filename
     */
    public function setFile($filename)
    {
        $this->fileName = $filename;
        $this->imageType = exif_imagetype($filename);
        
        if ($this->imageType == IMAGETYPE_JPEG) {
            $this->image = imagecreatefromjpeg($filename);
        } elseif ($this->imageType == IMAGETYPE_GIF) {
            $this->image = imagecreatefromgif($filename);
        } elseif ($this->imageType == IMAGETYPE_PNG) {
            $this->image = imagecreatefrompng($filename);
        }
    }

    /**
     * @param $filename
     * @param int $imageType
     * @param int $compression
     * @param null $permissions
     */
    public function save($filename, $imageType = IMAGETYPE_JPEG, $compression = 100, $permissions = null)
    {
        if ($imageType == IMAGETYPE_JPEG) {
            imagejpeg($this->image, $filename, $compression);
        } elseif ($imageType == IMAGETYPE_GIF) {
            imagegif($this->image, $filename);
        } elseif ($imageType == IMAGETYPE_PNG) {
            imagepng($this->image, $filename);
        }
        if ($permissions != null) {
            chmod($filename, $permissions);
        }
    }

    /**
     * @param int $imageType
     */
    public function output($imageType = IMAGETYPE_JPEG)
    {
        if ($imageType == IMAGETYPE_JPEG) {
            imagejpeg($this->image);
        } elseif($imageType == IMAGETYPE_GIF) {
            imagegif($this->image);
        } elseif($imageType == IMAGETYPE_PNG) {
            imagepng($this->image);
        }
    }

    public function rotate($degree)
    {
        if (is_int($degree)) {
            $this->removeRotation();

            if ($degree != 0) {
                $this->image = imagerotate($this->image, $degree, 0);
            }
        }
    }

    private function removeRotation()
    {
        if($this->imageType != IMAGETYPE_GIF) {
            $exif = exif_read_data($this->fileName);
            if (!empty($exif['Orientation'])) {
                switch ($exif['Orientation']) {
                    case 8:
                        $this->image = imagerotate($this->image, 90, 0);
                        break;
                    case 3:
                        $this->image = imagerotate($this->image, 180, 0);
                        break;
                    case 6:
                        $this->image = imagerotate($this->image, -90, 0);
                        break;
                }
            }
        }
    }
}
?>