<?php
namespace app\components\traits;

use yii\behaviors\TimestampBehavior;

/**
 * Trait CreatedUpdated
 * @package app\components\traits
 *
 * @property int|null $created
 * @property int|null $updated
 */
trait CreatedUpdated
{
    /**
     * @return false|string
     */
    public function getCreated()
    {
        return date('d.m.Y H:i:s', $this->created_at);
    }

    /**
     * @return false|string
     */
    public function getUpdated()
    {
        return date('d.m.Y H:i:s', $this->updated_at);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'class' => TimestampBehavior::class
        ];
    }
}