<?php

namespace app\components\traits;

use app\components\behaviors\FileBehavior;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\UploadedFile;

trait UploadingImage
{
    /**
     * Old image
     * @var string
     */
    public $delete_image;

    protected function getImageAttributeName()
    {
        return property_exists(self::className(), 'imageAttributeName') ? $this->imageAttributeName : 'image';
    }

    /**
     * @return null|string
     */
    public function getImagePath(): ?string
    {
        return $this->{$this->getImageAttributeName()} ? Url::base() . '/' . (property_exists(self::class, 'IMAGE_PATH') ? self::IMAGE_PATH : self::getDefaultPath()) . $this->{$this->getImageAttributeName()} : null;
    }

    /**
     * @return null|string
     */
    public function getThumbPath(): ?string
    {
        return $this->{$this->getImageAttributeName()} ? Url::base() . '/' . (property_exists(self::class, 'IMAGE_PATH') ? self::IMAGE_PATH : self::getDefaultPath()) . 'thumbs/' . $this->{$this->getImageAttributeName()} : null;
    }

    /**
     * @param array $options
     * @return null|string
     */
    public function getImageWithPath(array $options = []): ?string
    {
        return $this->{$this->getImageAttributeName()} ? Html::img($this->getImagePath(), $options) : null;
    }

    public function validateImage($attribute)
    {
        $file = UploadedFile::getInstance($this, $this->getImageAttributeName());
        if ($this->isNewRecord && $file == null) {
            $this->addError($attribute, 'Загрузите изображение');
            return false;
        }
        return true;
    }

    /**
     * @return string
     */
    static function getDefaultPath()
    {
        return 'uploads/images/' . Inflector::camel2id(substr(self::class, strripos(self::class, '\\') + 1)) . '/';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge([
            array_merge(
                [
                    'class' => FileBehavior::className(),
                    'fileAttrName' => $this->getImageAttributeName(),
                    'dir' => self::getDefaultPath(),
                    'deleteFileAttrName' => 'delete_image',
                    'rotation' => 0,
                    'thumbDir' => self::getDefaultPath() . 'thumbs/'
                ],
                $this->getFileBehaviorOptions())
        ], $this->getAdditionalBehaviors());
    }

    /**
     * Adding behaviors here
     * By default there is TimeStampBehavior
     * @return array
     */
    public function getAdditionalBehaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
        ];
    }

    public function getFileBehaviorOptions()
    {
        return [
            'width' => 1500,
        ];
    }
}