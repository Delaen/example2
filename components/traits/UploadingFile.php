<?php
namespace app\components\traits;

use app\components\behaviors\FileBehavior;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\helpers\Url;
use yii\web\UploadedFile;

trait UploadingFile
{
    /**
     * @var string
     */
    private $attributeName = 'file';
    /**
     * Adding behaviors here
     * By default there is TimeStampBehavior
     * @var array
     */
    private $behaviors = [
        [
            'class' => 'yii\behaviors\TimestampBehavior',
        ],
    ];
    /**
     * Old file
     * @var string
     */
    public $delete_file;

    protected function getDefaultPath()
    {
        return 'uploads/' . Inflector::camel2id(lcfirst(substr(self::class, strripos(self::class, '\\') + 1))) . '/';
    }

    /**
     * @return null|string
     */
    public function getFilePath(): ?string
    {
        return $this->{$this->attributeName} ? Url::base() . '/' . (property_exists(self::class, 'FILE_PATH') ? self::FILE_PATH : $this->getDefaultPath()) . $this->{$this->attributeName} : null;
    }

    /**
     * @param array $options
     * @return string|null
     */
    public function getFileLink(array $options = ['target' => '_blank']): ?string
    {
        return $this->{$this->attributeName} ? Html::a('Ссылка на файл', $this->getFilePath(), $options) : null;
    }

    public function validateFile($attribute)
    {
        $file = UploadedFile::getInstance($this, $this->attributeName);
        if ($this->isNewRecord && $file == null) {
            $this->addError($attribute, 'Загрузите файл');
            return false;
        }
        return true;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return array_merge([
            array_merge(
            [
                'class' => FileBehavior::className(),
                'fileAttrName' => $this->attributeName,
                'dir' => $this->getDefaultPath(),
                'deleteFileAttrName' => 'delete_file',
            ],
            isset($this->behaviorOptions) ? $this->behaviorOptions : [])
        ], $this->behaviors);
    }
}