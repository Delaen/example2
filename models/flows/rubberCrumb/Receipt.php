<?php

namespace app\models\flows\rubberCrumb;

use app\components\traits\CreatedUpdated;
use app\models\constants\Constant;
use Yii;
use app\models\WorkShift;

/**
 * This is the model class for table "rubber_crumb_receipts".
 *
 * @property int $id
 * @property int|null $work_shift_id
 * @property string|null $date
 * @property string|null $fraction
 * @property float|null $quantity
 * @property float|null $comment
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $workshop
 * @property string $updated
 * @property string $created
 *
 * @property string $fractionDescription
 * @property string $workshopDescription
 *
 * @property WorkShift $workShift
 */
class Receipt extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rubber_crumb_receipts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['workshop', 'date', 'fraction', 'quantity'], 'required'],
            [['work_shift_id', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['quantity'], 'number'],
            [['quantity'], 'compare', 'operator' => '>', 'compareValue' => 0],
            [['fraction'], 'string', 'max' => 30],
            [['fraction'], 'in', 'range' => Constant::rubberCrumbFractions()],
            [['comment'], 'string'],
            [['workshop'], 'in', 'range' => Constant::workshopsFilter()],
            [['work_shift_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkShift::className(), 'targetAttribute' => ['work_shift_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'work_shift_id' => 'Work Shift ID',
            'date' => 'Дата',
            'workshop' => 'Цех',
            'workshopDescription' => 'Цех',
            'fraction' => 'Фракция',
            'quantity' => 'Количеств, кг',
            'comment' => 'Примечание',
            'created' => 'Добавлено',
            'updated' => 'Изменено',
            'fractionDescription' => 'Фракция',
        ];
    }

    public function beforeSave($insert)
    {
        $this->date && $this->date = date('Y-m-d', strtotime($this->date));
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->date && $this->date = date('d.m.Y', strtotime($this->date));
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkShift()
    {
        return $this->hasOne(WorkShift::className(), ['id' => 'work_shift_id']);
    }

    /**
     * @return string
     */
    public function getFractionDescription()
    {
        return Constant::rubberCrumbFractions(true)[$this->fraction];
    }

    /**
     * @return mixed
     */
    public function getWorkshopDescription()
    {
        return Constant::workshops(true)[$this->workshop];
    }
}
