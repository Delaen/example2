<?php

namespace app\models\flows\rubberCrumb;

use app\components\traits\CreatedUpdated;
use app\models\constants\Constant;
use app\models\Order;
use Yii;

/**
 * This is the model class for table "rubber_crumb_cash_receipts".
 *
 * @property int $id
 * @property int|null $order_id
 * @property string $date
 * @property string $purpose
 * @property string $recipient
 * @property string $payment_type
 * @property float $summary
 * @property string|null $comment
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property string $purposeDescription
 * @property string $recipientDescription
 * @property string $paymentTypesDescription
 *
 * @property Order $order
 */
class CashReceipt extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rubber_crumb_cash_receipts';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [['date', 'purpose', 'recipient', 'payment_type', 'summary', 'order_id'], 'required'],
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['purpose'], 'in', 'range' => Constant::purposes()],
            [['payment_type'], 'in', 'range' => Constant::paymentTypes()],
            [['recipient'], 'in', 'range' => Constant::organizations()],
            [['summary'], 'number'],
            [['summary'], 'compare', 'operator' => '>', 'compareValue' => 0],
            [['summary'], 'compare', 'operator' => '<', 'compareValue' => ($this->order->debt + $this->summary),
                'message' => 'Пуступление не может быть больше остатка'],
            [['comment'], 'string'],
            [['purpose', 'recipient', 'payment_type'], 'string', 'max' => 30],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'date' => 'Дата',
            'purpose' => 'Назначение',
            'recipient' => 'Кому',
            'payment_type' => 'Описание',
            'summary' => 'Сумма, руб.',
            'comment' => 'Комментарий',
            'created' => 'Добавлено',
            'updated' => 'Изменено',
            'purposeDescription' => 'Назначение',
            'recipientDescription' => 'Кому',
            'paymentTypeDescription' => 'Описание',
        ];
    }

    public function beforeSave($insert)
    {
        $this->date && $this->date = date('Y-m-d', strtotime($this->date));
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->date && $this->date = date('d.m.Y', strtotime($this->date));
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return string
     */
    public function getPurposeDescription()
    {
        return Constant::purposes(true)[$this->purpose];
    }

    /**
     * @return string
     */
    public function getRecipientDescription()
    {
        return Constant::organizations(true)[$this->recipient];
    }

    /**
     * @return string
     */
    public function getPaymentTypeDescription()
    {
        return Constant::paymentTypes(true)[$this->payment_type];
    }
}
