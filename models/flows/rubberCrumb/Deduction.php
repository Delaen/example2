<?php

namespace app\models\flows\rubberCrumb;

use app\components\traits\CreatedUpdated;
use app\helpers\StockHelper;
use app\models\constants\Constant;
use app\models\Order;
use Yii;

/**
 * This is the model class for table "rubber_crumb_deduction".
 *
 * @property int $id
 * @property int|null $order_id
 * @property string|null $date
 * @property string|null $fraction
 * @property float|null $quantity
 * @property float|null $comment
 * @property string $workshop
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property string $fractionDescription
 * @property string $workshopDescription
 *
 * @property Order $order
 */
class Deduction extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rubber_crumb_deduction';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['workshop', 'date', 'fraction', 'quantity'], 'required'],
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['quantity'], 'number'],
            [['quantity'], 'compare', 'operator' => '>', 'compareValue' => 0],
            [['fraction'], 'string', 'max' => 30],
            [['fraction'], 'in', 'range' => Constant::rubberCrumbFractions()],
            [['fraction'], 'validateFraction'],
            [['quantity'], 'validateQuantity'],
            [['comment'], 'string'],
            [['workshop'], 'in', 'range' => Constant::workshopsFilter()],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'date' => 'Дата',
            'fraction' => 'Фракция',
            'workshop' => 'Цех',
            'workshopDescription' => 'Цех',
            'fractionDescription' => 'Фракция',
            'quantity' => 'Количество, кг',
            'comment' => 'Примечание',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function beforeSave($insert)
    {
        $this->date && $this->date = date('Y-m-d', strtotime($this->date));
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->date && $this->date = date('d.m.Y', strtotime($this->date));
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * Validates quantity
     */
    public function validateQuantity()
    {
        //Проверка на то что количество расхода не больше чем количество остатка по заказу
        if ($this->order && ($this->quantity > $this->order->shippingDebt)) {
            $this->addError('quantity', 'Указанное количество больше остатка по заказу');
        }

        //Проверка, что на складе достаточно крошки для отгрузки
        if ($this->quantity > StockHelper::rubberCrumbStock($this->fraction, $this->workshop, $this->date)) {
            $this->addError('quantity', 'На складе недостаточно крошки');
        }
    }

    /**
     * Validates fraction
     */
    public function validateFraction()
    {
        if ($this->order && ($this->fraction !== $this->order->fraction)) {
            $this->addError('fraction', 'Фракция расхода не должна отличатсья от фракции заказа');
        }
    }

    /**
     * @return mixed
     */
    public function getFractionDescription()
    {
        return Constant::rubberCrumbFractions(true)[$this->fraction];
    }

    /**
     * @return mixed
     */
    public function getWorkshopDescription()
    {
        return Constant::workshops(true)[$this->workshop];
    }
}
