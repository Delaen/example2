<?php

namespace app\models\flows\rubberCrumb\search;

use app\models\constants\Constant;
use app\models\flows\rubberCrumb\Deduction;
use app\models\flows\rubberCrumb\Receipt;
use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use Yii;

/**
 * Cost represents the model behind the search form of `app\models\Cost`.
 */
class StockSearch extends Model
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $queryReceipt = Receipt::find()->select(['id', 'source' => 'work_shift_id', 'workshop', 'date', 'fraction', 'quantity', 'comment', 'created_at', 'updated_at'])
            ->addSelect(new Expression("'" . Constant::FLOW_RECEIPT ."' as type"))
            ->alias('receipt');
        $queryDeduction = Deduction::find()->select(['id', 'source' => 'order_id', 'workshop', 'date', 'fraction', 'quantity', 'comment', 'created_at', 'updated_at'])
            ->addSelect(new Expression("'" . Constant::FLOW_DEDUCTION ."' as type"))
            ->alias('deduction');

        if (Yii::$app->user->identity->getRole() == \app\models\constants\Roles::MASTER) {
            $queryReceipt->andWhere(['workshop' => Yii::$app->user->identity->user->workshop]);
            $queryDeduction->andWhere(['workshop' => Yii::$app->user->identity->user->workshop]);
        }

        $query = $queryReceipt->union($queryDeduction->createCommand()->rawSql);
        // add conditions that should always apply here

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql . ' ORDER BY date DESC',
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
