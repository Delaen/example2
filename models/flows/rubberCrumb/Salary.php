<?php

namespace app\models\flows\rubberCrumb;

use app\components\traits\CreatedUpdated;
use app\models\catalog\Employee;
use app\models\catalog\ModelWork;
use app\models\WorkShift;
use Yii;

/**
 * This is the model class for table "rubber_crumb_salary".
 *
 * @property int $id
 * @property int|null $employee_id
 * @property float|null $salary
 * @property int|null $additional_work_id
 * @property int|null $work_shift_id
 * @property float|null $additional_work_salary
 * @property string|null $comment
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $updated
 * @property string $created
 *
 * @property string $employeeFio
 * @property string $additionalWorkName
 *
 * @property Employee $employee
 * @property ModelWork $additionalWork
 * @property WorkShift $workShift;
 */
class Salary extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rubber_crumb_salary';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'salary', 'work_shift_id'], 'required'],
            [['additional_work_salary'], 'compare', 'operator' => '>', 'compareValue' => 0, 'skipOnEmpty'  => false,
                'when' => function($model) {
                return $this->additional_work_id;
            }, 'whenClient' => 'function(attribute, value) {
                    return $("#salary-additional_work_id").val();
             }'],
            [['employee_id', 'additional_work_id', 'created_at', 'updated_at'], 'integer'],
            [['salary', 'additional_work_salary'], 'number'],
            [['salary', 'additional_work_salary'], 'filter', 'filter' => 'abs', 'skipOnEmpty' => true],
            [['salary'], 'compare', 'operator' => '>', 'compareValue' => 0],
            [['comment'], 'string'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['additional_work_id'], 'exist', 'skipOnError' => true, 'targetClass' => ModelWork::className(), 'targetAttribute' => ['additional_work_id' => 'id']],
            [['work_shift_id'], 'exist', 'skipOnError' => true, 'targetClass' => WorkShift::className(), 'targetAttribute' => ['work_shift_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Сотрудник',
            'employeeFio' => 'Сотрудник',
            'salary' => 'З/п за крошку',
            'additional_work_id' => 'Доп.работа',
            'additionalWorkName' => 'Доп.работа',
            'additional_work_salary' => 'З/п за доп. работу',
            'comment' => 'Примечание',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено',
            'created' => 'Добавлено',
            'updated' => 'Изменено',
        ];
    }

    public function beforeSave($insert)
    {
        !$this->additional_work_id && $this->additional_work_salary = null;
        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdditionalWork()
    {
        return $this->hasOne(ModelWork::className(), ['id' => 'additional_work_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWorkShift()
    {
        return $this->hasOne(Workshift::className(), ['id' => 'work_shift_id']);
    }

    /**
     * @return string
     */
    public function getEmployeeFio()
    {
        return $this->employee->fio;
    }

    public function getAdditionalWorkName()
    {
        return $this->additional_work_id ? $this->additionalWork->name : null;
    }
}
