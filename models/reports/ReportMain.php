<?php

namespace app\models\reports;

use app\models\catalog\Employee;
use app\models\constants\Constant;
use app\models\flows\rubberCrumb\Deduction;
use app\models\flows\rubberCrumb\Receipt;
use app\models\flows\rubberCrumb\Salary;
use app\models\WorkShift;
use yii\base\Model;
use yii\data\SqlDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class ReportMain
 * @package app\models\search
 */
class ReportMain extends Model
{
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var string
     */
    public $dateTo;
    /**
     * @var string
     */
    public $workshop;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:d.m.Y'],
            [['workshop'], 'in', 'range' => Constant::workshops()],
        ];
    }

    public function attributeLabels()
    {
        return [
            'dateFrom' => 'От',
            'dateTo' => 'До',
            'workshop' => 'Цех',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return SqlDataProvider
     */
    public function search($params)
    {
        $this->load($params);
     /*
      * /////Эмуляция запроса
       select dates.date,
       (SELECT sum(quantity) from rubber_crumb_deduction where date=dates.date) as deduction,
       (SELECT sum(quantity) from rubber_crumb_receipts where date=dates.date) as receipt,
       salary.s1,salary.s2
from (SELECT DISTINCT date from rubber_crumb_deduction UNION
      SELECT DISTINCT date from rubber_crumb_receipts UNION
      SELECT DISTINCT date from work_shift) as dates

left join (SELECT sum(salary) as s1,sum(additional_work_salary) as s2, `work_shift`.date from rubber_crumb_salary
         INNER JOIN `work_shift` ON `rubber_crumb_salary`.`work_shift_id` = `work_shift`.`id` group by date) salary on salary.date=dates.date
order by date DESC;
     */
        //Main query
        $query = new Query();

        //Выбираем все возможные даты, формируем таблицу с одним столбцом 'date'
        $queryFrom = (new Query())->select('date')->distinct()->from(Deduction::tableName());
        $subQueryFromReceipt = (new Query())->select('date')->distinct()->from(Receipt::tableName());
        $subQueryFromWorkshift = ((new Query())->select('date')->distinct()->from(WorkShift::tableName()));

        $queryFrom->union($subQueryFromReceipt);
        $queryFrom->union($subQueryFromWorkshift);

        //Каждый запрос, отдельный столбец в общем селекте
        $queryReceipt = Receipt::find()->select('sum(quantity)')->where('date=dates.date');
        $queryDeduction = Deduction::find()->select('sum(quantity)')->where('date=dates.date');

        //ФИЛЬТР ПО ЦЕХУ
        if ($this->workshop && $this->workshop != Constant::WORKSHOP_ALL) {
            $queryFrom->andWhere(['workshop' => $this->workshop]);
            $subQueryFromReceipt->andWhere(['workshop' => $this->workshop]);
            $subQueryFromWorkshift->andWhere(['workshop' => $this->workshop]);
        }


        //добавляем столбы з/п и з/п за доп.работы
        $querySalary = Salary::find()->select([
            'salary' => 'sum(salary)',
            'addSalary' => 'sum(additional_work_salary)',
            WorkShift::tableName() . '.date'
        ])->innerJoinWith('workShift')
        ->groupBy('date');

        $query->select([
            'dates.date',
            'receipt' => '(' . $queryReceipt->createCommand()->rawSql . ')',
            'deduction' => '(' . $queryDeduction->createCommand()->rawSql . ')',
            'st.salary',
            'st.addSalary',
        ]);
        $query->from('(' . (new Expression($queryFrom->createCommand()->rawSql) . ') as dates'));
        $query->leftJoin(['st' => $querySalary], 'st.date=dates.date');
        $query->orderBy('date DESC');

        if ($this->dateFrom) {
            $query->andWhere(['>=', 'dates.date', date('Y-m-d', strtotime($this->dateFrom))]);
        }
        if ($this->dateTo) {
            $query->andWhere(['<=', 'dates.date', date('Y-m-d', strtotime($this->dateTo))]);
        }

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'sort' => false,
        ]);

        if (!$this->validate()) {
            return $dataProvider;
        }

        return $dataProvider;
    }
}
