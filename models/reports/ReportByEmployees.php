<?php

namespace app\models\reports;

use app\models\catalog\Employee;
use app\models\constants\Constant;
use app\models\flows\rubberCrumb\Salary;
use app\models\WorkShift;
use yii\base\Model;
use yii\data\ActiveDataProvider;

/**
 * Class ReportByEmployees
 * @package app\models\search
 */
class ReportByEmployees extends Model
{
    /**
     * @var string
     */
    public $dateFrom;
    /**
     * @var string
     */
    public $dateTo;
    /**
     * @var string
     */
    public $workshop;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:d.m.Y'],
            [['workshop'], 'in', 'range' => Constant::workshops()],
        ];
    }

    public function attributeLabels()
    {
        return [
            'dateFrom' => 'От',
            'dateTo' => 'До',
            'workshop' => 'Цех',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Salary::find()->orderBy('date DESC, ' . Salary::tableName() . '.created_at DESC')
            ->innerJoinWith('employee')->innerJoinWith('workShift');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            return $dataProvider;
        }

        if ($this->dateFrom) {
            $query->andWhere(['>=', WorkShift::tableName() . '.date', date('Y-m-d', strtotime($this->dateFrom))]);
        }
        if ($this->dateTo) {
            $query->andWhere(['<=', WorkShift::tableName() . '.date', date('Y-m-d', strtotime($this->dateTo))]);
        }

        if ($this->workshop && $this->workshop != Constant::WORKSHOP_ALL) {
            $query->andWhere([Employee::tableName() . '.workshop' => $this->workshop]);
        }

        return $dataProvider;
    }
}
