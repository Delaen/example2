<?php

namespace app\models;

use app\components\traits\UploadingFile;
use Yii;
use yii\helpers\Html;

/**
 * This is the model class for table "order_file".
 *
 * @property int $id
 * @property int $order_id
 * @property string $filename
 * @property string $file
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property Order $order
 */
class OrderFile extends \yii\db\ActiveRecord
{
    use UploadingFile;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['order_id', 'filename'], 'required'],
            [['order_id', 'created_at', 'updated_at'], 'integer'],
            [['filename', 'file'], 'string', 'max' => 255],
            [['file'], 'file', 'extensions' => 'pdf, doc, docx', 'maxSize' => 10 * 1024 * 1024],
            [['file'], 'validateFile', 'skipOnEmpty' => false],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Номер заказа',
            'filename' => 'Имя файла',
            'file' => 'Файл',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @param array $options
     * @return string|null
     */
    public function getFileLink(array $options = ['target' => '_blank']): ?string
    {
        return $this->{$this->attributeName} ? Html::a($this->filename, $this->getFilePath(), $options) : null;
    }
}
