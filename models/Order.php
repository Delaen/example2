<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use app\models\catalog\Counterparty;
use app\models\constants\Constant;
use app\models\flows\rubberCrumb\CashReceipt;
use app\models\flows\rubberCrumb\Deduction;
use Yii;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * This is the model class for table "orders".
 *
 * @property int $id
 * @property int|null $counterparty_id
 * @property string $status
 * @property string $goods_type
 * @property string $fraction
 * @property float|null $quantity
 * @property float|null $summary
 * @property string|null $comment
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $updated
 * @property string $created
 *
 * @property string $shortDescription
 * @property string $counterpartyName
 * @property string $statusDescription
 * @property string $productionStatus
 * @property string $productionStatusDescription
 * @property float $payedAmount
 * @property float $debt
 * @property float $shippedQnt
 * @property float $shippingDebt
 *
 * @property Counterparty $counterparty
 * @property CashReceipt[] $cashReceipts
 * @property Deduction[] $deductions
 * @property Deduction[] $files
 */
class Order extends \yii\db\ActiveRecord
{
    use CreatedUpdated;

    //Статусы продажи
    const STATUS_BILL_SENT = 'bill-sent';
    const STATUS_PRE_PAYED = 'pre-payed';
    const STATUS_PAYED = 'payed';
    const STATUS_CANCELED = 'canceled';

    //Статусы производства

    const PRODUCTION_STATUS_NEW = 'prod-new';
    const PRODUCTION_STATUS_DONE = 'prod-done';

    //Сценарии
    const SCENARIO_STATUS = 'status';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'orders';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['counterparty_id', 'created_at', 'updated_at'], 'integer'],
            [['fraction', 'counterparty_id', 'quantity', 'summary'], 'required'],
            [['quantity', 'summary'], 'number'],
            [['quantity', 'summary'], 'filter', 'filter' => 'abs', 'skipOnEmpty' => true],
            ['summary', 'compare', 'operator' => '>', 'compareValue' => $this->payedAmount, 'message' => 'Сумма не может быть меньше текущих поступлений'],
            ['quantity', 'compare', 'operator' => '>', 'compareValue' => $this->shippedQnt, 'message' => 'Количество не может быть меньше уже отгруженной суммы'],
            [['comment'], 'string'],
            [['goods_type', 'fraction', 'status'], 'string', 'max' => 30],
            [['goods_type'], 'default', 'value' => Constant::GOODS_TYPE_CRUMB],
            [['status'], 'in', 'range' => self::statuses()],
            [['status'], 'default', 'value' => self::STATUS_BILL_SENT],
            [['counterparty_id'], 'exist', 'skipOnError' => true, 'targetClass' => Counterparty::className(), 'targetAttribute' => ['counterparty_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'counterparty_id' => '№ контрагента',
            'counterpartyName' => 'Контрагент',
            'status' => 'Статус продажи',
            'statusDescription' => 'Статус продажи',
            'productionStatus' => 'Статус производства',
            'goods_type' => 'Тип изделия',
            'fraction' => 'Фракция',
            'quantity' => 'Количество, кг',
            'summary' => 'Сумма, руб.',
            'comment' => 'Примечание',
            'created' => 'Добавлено',
            'updated' => 'Изменено',
            'shortDescription' => 'Заказ',
            'productionStatusDescription' => 'Статус производства',
        ];
    }

    public function scenarios()
    {
        $scenarios = [
            self::SCENARIO_STATUS =>
                ['status'],
        ];
        return array_merge(parent::scenarios(), $scenarios);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCounterparty()
    {
        return $this->hasOne(Counterparty::className(), ['id' => 'counterparty_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashReceipts()
    {
        return $this->hasMany(CashReceipt::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDeductions()
    {
        return $this->hasMany(Deduction::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(OrderFile::className(), ['order_id' => 'id']);
    }

    /**
     * @return string|null
     */
    public function getCounterpartyName()
    {
        return $this->counterparty ? $this->counterparty->name : null;
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return Constant::workGoodsType(true)[$this->goods_type] . ' ' .
            Constant::rubberCrumbFractions(true)[$this->fraction];
    }

    /**
     * @return mixed
     */
    public function getStatusDescription()
    {
        return self::statuses(true)[$this->status];
    }


    /**
     * @return float
     */
    public function getShippedQnt(): float
    {
        $qnt = 0.0;
        foreach ($this->deductions as $deduction) {
            $qnt += $deduction->quantity;
        }

        return $qnt;
    }

    /**
     * @return float
     */
    public function getShippingDebt(): float
    {
        return $this->quantity - $this->shippedQnt;
    }

    /**
     * @return mixed
     */
    public function getProductionStatus()
    {
        return ($this->quantity === $this->shippedQnt) ? self::PRODUCTION_STATUS_DONE : self::PRODUCTION_STATUS_NEW;
    }

    /**
     * @return mixed
     */
    public function getProductionStatusDescription()
    {
        return self::productionStatuses(true)[$this->productionStatus];
    }

    /**
     * @return float
     */
    public function getPayedAmount(): float
    {
        $amount = 0.0;
        foreach ($this->cashReceipts as $receipt) {
            $amount += $receipt->summary;
        }

        return $amount;
    }

    /**
     * @return float
     */
    public function getDebt():float
    {
        return $this->summary - $this->payedAmount;
    }

    /**
     * @param bool $withDescription
     * @return array
     */
    static function statuses($withDescription = false)
    {
        return $withDescription ?
            [
                self::STATUS_BILL_SENT => 'Счет отправлен',
                self::STATUS_PRE_PAYED => 'Получена предоплата',
                self::STATUS_PAYED => 'Получена оплата',
                self::STATUS_CANCELED => 'Сделка отменена',
            ] :
            [
                self::STATUS_BILL_SENT, self::STATUS_PRE_PAYED, self::STATUS_PAYED, self::STATUS_CANCELED
            ];
    }

    /**
     * @param bool $withDescription
     * @return array
     */
    static function productionStatuses($withDescription = false)
    {
        return $withDescription ?
            [
                self::PRODUCTION_STATUS_NEW => 'Новая',
                self::PRODUCTION_STATUS_DONE => 'Завершено',
            ] :
            [
                self::PRODUCTION_STATUS_NEW, self::PRODUCTION_STATUS_DONE
            ];
    }

    /**
     * @param bool $ready
     * @return ActiveQuery
     */
    static function findByShipping(bool $ready = false): ActiveQuery
    {
        $query = self::find()->orderBy('updated_at DESC');
        $query->where([
            $ready ? '=' : '>',
            'quantity',
            (new Expression('IFNULL(' . Deduction::find()->select('sum(quantity)')
                ->where('`order_id` = ' . self::tableName() . '.id')))->expression . ', 0)'
        ]);

        return $query;
    }
}
