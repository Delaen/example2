<?php

namespace app\models;

use Yii;
use app\helpers\CommonHelper;
use yii\web\IdentityInterface;


/**
 * This is the model class for table "identity".
 *
 * @property string $id
 * @property string $login
 * @property string $password
 * @property string $status
 * @property string $checkPassword
 * @property string $auth_key
 *
 * @property User $user
 */
class Identity extends \yii\db\ActiveRecord implements IdentityInterface
{
    /**
     * Статусы
     */
    const STATUS_ACTIVE = 'A';
    const STATUS_BLOCKED = 'B';

    /**
     * Сценарии
     */
    const SCENARIO_CHANGE_PASS = 'change_pass';

    /**
     * Поле "Повторите паролЬ"
     * @var string
     */
    public $checkPassword;

    /**
     * Identity constructor.
     * @param array $config
     * @throws \Exception
     */
    public function __construct(array $config = [])
    {
        parent::__construct($config);
        $this->id = CommonHelper::generateIdx();
        $this->auth_key = Yii::$app->security->generateRandomString(36);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'identity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['login', 'password'], 'string', 'max' => 255],
            [['password', 'checkPassword'], 'required', 'on' => self::SCENARIO_CHANGE_PASS],
            [['password', 'checkPassword'], 'required', 'when' => function($model, $attribute){return $model->isNewRecord;}],
            [['checkPassword'], 'compare', 'compareAttribute' => 'password', 'on' => [self::SCENARIO_CHANGE_PASS] ],
            [['checkPassword'], 'compare', 'compareAttribute' => 'password', 'when' => function($model, $attribute){return $model->isNewRecord;}],
            [['status'], 'default', 'value' => self::STATUS_ACTIVE],
            [['login'], 'required'],
            [['login'], 'unique'],
        ];
    }

    public function scenarios()
    {
        $scenarios = [
            self::SCENARIO_CHANGE_PASS => ['password', 'checkPassword'],
        ];
        return array_merge($scenarios, parent::scenarios());
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Логин',
            'password' => 'Пароль',
            'checkPassword' => 'Повторите пароль',
            'status' => 'Статус',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if ($insert || ($this->scenario === self::SCENARIO_CHANGE_PASS)) {
            $this->password = Yii::$app->security->generatePasswordHash($this->password);
        }
        return parent::beforeSave($insert);
    }

    /**
     * @param int|string $id
     * @return null|IdentityInterface|static
     */
    public static function findIdentity($id)
    {
        return self::findOne($id);
    }

    /**
     * @param $login
     * @return null|static
     */
    public static function findByLogin($login)
    {
        $identity = self::findOne(['login' => $login, 'status' => self::STATUS_ACTIVE]);
        return $identity ?: null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->auth_key === $authKey;
    }


    /**
     * @param string $password
     * @return bool
     * @throws \yii\base\Exception
     */
    public function validatePassword(string $password): bool
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['identity' => 'id']);
    }

    /**
     * @return $this
     */
    public function getStatusDescription()
    {
        return self::getStatuses()[$this->status];
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return null|string
     */
    public function getRole() : ?string
    {
        $role = Yii::$app->authManager->getRolesByUser($this->id);
        if (is_array($role)) {
            return key($role);
        }
        return null;
    }

    /**
     * @return null|string
     */
    public function getRoleDescription() : ?string
    {
        $role = Yii::$app->authManager->getRolesByUser($this->id);
        if (is_array($role)) {
            return User::getRoles()[key($role)];
        }
        return null;
    }

    /**
     * @return array
     */
    static function getStatuses(): array
    {
        return [
            self::STATUS_ACTIVE => 'Активный',
            self::STATUS_BLOCKED => 'Заблокированный',
        ];
    }
}
