<?php

namespace app\models\catalog;

use app\components\traits\CreatedUpdated;
use app\models\constants\Constant;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "employee".
 *
 * @property int $id
 * @property string $fio
 * @property string $workshop
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $created
 * @property string $updated
 * @property string $workshopDescription
 */
class Employee extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fio', 'workshop'], 'required'],
            [['fio'], 'unique'],
            [['created_at', 'updated_at'], 'integer'],
            [['fio', 'workshop'], 'string', 'max' => 255],
            [['workshop'], 'in', 'skipOnEmpty' => false, 'range' => Constant::workshopsFilter(false)],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'fio' => 'ФИО',
            'workshop' => 'Цех',
            'workshopDescription' => 'Цех',
            'created' => 'Создано',
            'updated' => 'Изменено',
        ];
    }

    /**
     * @return mixed
     */
    public function getWorkshopDescription()
    {
        return $this->workshop ? Constant::workshops(true)[$this->workshop] : null;
    }

    /**
     * @return array
     */
    static function ddl(): array
    {
        return ArrayHelper::map(self::find()->orderBy('fio')->all(), 'id', 'fio');
    }
}
