<?php

namespace app\models\catalog;

use app\components\traits\CreatedUpdated;
use app\models\constants\Constant;
use app\models\Cost as CostReal;
use Yii;
use yii\helpers\ArrayHelper;
use function foo\func;

/**
 * This is the model class for table "costs_catalog".
 *
 * @property int $id
 * @property string $name
 * @property string $unit_of_measurement
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $created
 * @property string $updated
 *
 * @property CostReal[] $costs
 */
class Cost extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'costs_catalog';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'unit_of_measurement'], 'required'],
            [['name'], 'unique'],
            [['unit_of_measurement'], 'in', 'range' => Constant::units(false)],
            [['created_at', 'updated_at'], 'integer'],
            [['name', 'unit_of_measurement'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'name' => 'Название',
            'unit_of_measurement' => 'Ед. изм.',
            'unit' => 'Ед. изм.',
            'created' => 'Создано',
            'updated' => 'Изменено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCosts()
    {
        return $this->hasMany(CostReal::className(), ['cost_catalog_id' => 'id']);
    }

    /**
     * @return mixed
     */
    public function getUnit()
    {
        return Constant::units(true)[$this->unit_of_measurement];
    }

    static function ddl()
    {
        return ArrayHelper::map(self::find()->orderBy('name')->all(), 'id', function($model) {
            return $model->name . '(ед.изм - ' . Constant::units(true)[$model->unit_of_measurement] .')';
        } );
    }
}
