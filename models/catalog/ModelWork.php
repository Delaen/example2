<?php

namespace app\models\catalog;

use app\components\traits\CreatedUpdated;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "model_work".
 *
 * @property int $id
 * @property string $name
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $created
 * @property string $updated
 */
class ModelWork extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'model_work';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['created_at', 'updated_at'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'name' => 'Название',
            'created' => 'Создано',
            'updated' => 'Изменено',
        ];
    }

    /**
     * @return array
     */
    static function ddl():array
    {
        return ArrayHelper::map(self::find()->orderBy('name')->all(), 'id', 'name');
    }

    /**
     * @return array
     */
    static function ddlWithNoSelect()
    {
        $result = ['' => '-'];
        foreach (self::ddl() as $id => $workName) {
            $result[$id] = $workName;
        }

        return $result;
    }
}
