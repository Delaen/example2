<?php

namespace app\models\catalog;

use app\components\traits\CreatedUpdated;
use Yii;
use app\models\User;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "counterparty".
 *
 * @property int $id
 * @property string $name
 * @property string|null $contact_person
 * @property string|null $phone_number
 * @property string|null $email
 * @property int|null $creator_id
 * @property string|null $comment
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $created
 * @property string $updated
 *
 * @property User $creator
 */
class Counterparty extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'counterparty';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'creator_id'], 'required'],
            [['name'], 'unique'],
            [['creator_id', 'created_at', 'updated_at'], 'integer'],
            [['comment'], 'string'],
            [['name', 'contact_person', 'phone_number', 'email'], 'string', 'max' => 255],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'name' => 'Название',
            'contact_person' => 'Контактное лицо',
            'phone_number' => 'Номер телефона',
            'email' => 'Email',
            'creator_id' => 'Ответственный',
            'comment' => 'Примечание',
            'created' => 'Создано',
            'updated' => 'Изменено',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return array
     */
    static function ddl()
    {
        return ArrayHelper::map(self::find()->orderBy('name')->all(), 'id', 'name');
    }
}
