<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use app\models\constants\Constant;
use app\models\flows\rubberCrumb\Receipt;
use app\models\flows\rubberCrumb\Salary;
use Yii;

/**
 * This is the model class for table "work_shift".
 *
 * @property int $id
 * @property string $date
 * @property string $type
 * @property string $goods_type
 * @property string $workshop
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string $updated
 * @property string $created
 *
 * @property string $typeDescription
 * @property string $goodsTypeDescription
 * @property float $salarySum
 * @property float $additionalSalarySum
 * @property float $totalSalarySum
 * @property string $shortDescription
 * @property string $workshopDescription
 *
 * @property Receipt[] $receipts
 * @property Salary[] $salaries
 */
class WorkShift extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'work_shift';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'type', 'workshop'], 'required'],
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['goods_type'], 'default', 'value' => Constant::GOODS_TYPE_CRUMB],
            [['goods_type'], 'in', 'range' => Constant::workGoodsType()],
            [['type'], 'in', 'range' => Constant::workShifts()],
            [['type'], 'validateType'],
            [['workshop'], 'in', 'range' => Constant::workshopsFilter()],
            [['created_at', 'updated_at'], 'integer'],
            [['type', 'goods_type'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date' => 'Дата',
            'type' => 'Вид смены',
            'typeDescription' => 'Вид смены',
            'workshop' => 'Цех',
            'workshopDescription' => 'Цех',
            'goods_type' => 'Товар',
            'goodsTypeDescription' => 'Товар',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created' => 'Добавлено',
            'updated' => 'Изменено',
            'totalSalarySum' => 'Выплачено',
        ];
    }

    public function beforeSave($insert)
    {
        if (array_key_exists('date', $this->dirtyAttributes)) {
            Receipt::updateAll(['date' => date('Y-m-d', strtotime($this->date))], ['work_shift_id' => $this->id]);
        }
        if (array_key_exists('workshop', $this->dirtyAttributes)) {
            Receipt::updateAll(['workshop' => $this->workshop], ['work_shift_id' => $this->id]);
        }

        $this->date && $this->date = date('Y-m-d', strtotime($this->date));
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->date && $this->date = date('d.m.Y', strtotime($this->date));
        parent::afterFind();
    }

    /**
     * Validates uniqueness of the shift type
     */
    public function validateType()
    {
        $query = self::find()
            ->where(['date' => date('Y-m-d', strtotime($this->date))])
            ->andWhere(['workshop' => $this->workshop])
            ->andWhere(['type' => $this->type]);
        !$this->isNewRecord && $query->andWhere(['!=', 'id', $this->id]);
        if ($query->one()) {
            $this->addError('type', 'В одну дату может быть только одна дневная и одна ночная смена');
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceipts()
    {
        return $this->hasMany(Receipt::className(), ['work_shift_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalaries()
    {
        return $this->hasMany(Salary::className(), ['work_shift_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getShortDescription()
    {
        return Constant::workShifts(true)[$this->type] . " от $this->date";
    }

    /**
     * @return mixed
     */
    public function getTypeDescription()
    {
        return Constant::workShifts(true)[$this->type];
    }

    /**
     * @return mixed
     */
    public function getGoodsTypeDescription()
    {
        return Constant::workGoodsType(true)[$this->goods_type];
    }

    /**
     * @return mixed
     */
    public function getWorkshopDescription()
    {
        return Constant::workshops(true)[$this->workshop];
    }

    /**
     * @return float
     */
    public function getSalarySum(): float
    {
        $sum = 0.0;
        foreach ($this->salaries as $salary) {
            $sum += $salary->salary;
        }

        return $sum;
    }

    /**
     * @return float
     */
    public function getAdditionalSalarySum(): float
    {
        $sum = 0.0;
        foreach ($this->salaries as $salary) {
            $sum += $salary->additional_work_salary;
        }

        return $sum;
    }

    /**
     * @return float
     */
    public function getTotalSalarySum():float
    {
        return $this->salarySum + $this->additionalSalarySum;
    }
}
