<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use app\models\constants\Constant;
use app\models\constants\Roles;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $identity
 * @property string $name
 * @property string $status
 * @property string $workshop
 * @property string $role
 *
 * @property Identity $entity
 */
class User extends \yii\db\ActiveRecord
{
    use CreatedUpdated;

    public $role;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'unique'],
            [['identity'], 'string', 'max' => 36],
            [['name'], 'string', 'max' => 255],
            [['workshop'], 'string', 'max' => 50],
            [['workshop'], 'default', 'value' => Constant::WORKSHOP_ALL],
            [['workshop'], 'in', 'range' => Constant::workshops(false)],
            [['role'], 'in', 'range' => Roles::getAssignmentList(false)],
            [['identity'], 'exist', 'skipOnError' => true, 'targetClass' => Identity::className(), 'targetAttribute' => ['identity' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'identity' => 'Логин',
            'name' => 'ФИО',
            'role' => 'Роль',
            'workshop' => 'Цех',
            'workshopDescription' => 'Цех',
            'created' => 'Создан',
            'updated' => 'Обновлен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Identity::className(), ['id' => 'identity']);
    }

    /**
     * @return null|string
     */
    public function getRole() : ?string
    {
        $role = Yii::$app->authManager->getRolesByUser($this->entity->id);
        if (is_array($role)) {
            return key($role);
        }
        return null;
    }

    /**
     * @return null|string
     */
    public function getRoleDescription() : ?string
    {
        $role = Yii::$app->authManager->getRolesByUser($this->entity->id);
        if (is_array($role)) {
            return Roles::getList(true)[key($role)];
        }
        return null;
    }

    public function block()
    {
        $this->entity->status = Identity::STATUS_BLOCKED;
        return $this->entity->save();
    }

    public function activate()
    {
        $this->entity->status = Identity::STATUS_ACTIVE;
        return $this->entity->save();
    }

    /**
     * @return mixed
     */
    public function getWorkshopDescription()
    {
        return $this->workshop ? Constant::workshops(true)[$this->workshop] : null;
    }

    /**
     * @return array
     */
    static function ddlMarketers()
    {
        $marketers = self::find()->select([self::tableName(). '.id', 'name'])
            ->innerJoin(Identity::tableName(), self::tableName(). '.identity=' . Identity::tableName() . '.id')
            ->innerJoin('{{auth_assignment}}', Identity::tableName(). '.[[id]]={{auth_assignment}}.[[user_id]] COLLATE utf8_unicode_ci')
            ->where(['auth_assignment.item_name' => Roles::MARKETER])
            ->orderBy(self::tableName() . '.name')
            ->all();
        return ArrayHelper::map($marketers, 'id', 'name');
    }
}
