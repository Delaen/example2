<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use app\models\catalog\Cost as CostCatalog;
use app\models\constants\Constant;
use Yii;

/**
 * This is the model class for table "costs".
 *
 * @property int $id
 * @property string $date
 * @property int|null $cost_catalog_id
 * @property float|null $quantity
 * @property string|null $comment
 * @property string $workshop
 * @property int|null $created_at
 * @property int|null $updated_at
 *
 * @property string $workshopDescription
 *
 * @property CostCatalog $costCatalog
 */
class Cost extends \yii\db\ActiveRecord
{
    use CreatedUpdated;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'costs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'cost_catalog_id', 'quantity', 'workshop'], 'required'],
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['cost_catalog_id', 'created_at', 'updated_at'], 'integer'],
            [['workshop'], 'in', 'range' => Constant::workshopsFilter()],
            [['quantity'], 'number'],
            [['quantity'], 'compare', 'operator' => '>', 'compareValue' => 0],
            [['comment'], 'string'],
            [['cost_catalog_id'], 'exist', 'skipOnError' => true, 'targetClass' => CostCatalog::className(), 'targetAttribute' => ['cost_catalog_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'date' => 'Дата',
            'cost_catalog_id' => 'Расход',
            'name' => 'Расход',
            'workshop' => 'Цех',
            'quantity' => 'Количество',
            'comment' => 'Примечание',
            'created' => 'Создано',
            'updated' => 'Обновлено',
        ];
    }

    public function beforeSave($insert)
    {
        $this->date && $this->date = date('Y-m-d', strtotime($this->date));
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->date && $this->date = date('d.m.Y', strtotime($this->date));
        parent::afterFind();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCostCatalog()
    {
        return $this->hasOne(CostCatalog::className(), ['id' => 'cost_catalog_id']);
    }

    /**
     * @return string|null
     */
    public function getName()
    {
        return $this->costCatalog ? $this->costCatalog->name : null;
    }

    /**
     * @return mixed
     */
    public function getWorkshopDescription()
    {
        return Constant::workshops(true)[$this->workshop];
    }
}
