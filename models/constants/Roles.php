<?php

namespace app\models\constants;

abstract class Roles
{
    const ADMIN = 'admin';
    const CEO = 'ceo';
    const MASTER = 'master';
    const ACCOUNTANT = 'accountant';
    const MARKETER = 'marketer';

    /**
     * @param bool $withDescription
     * @return array
     */
    static function getList(bool $withDescription = false)
    {
        return $withDescription ?
            [
                self::ADMIN => 'Администратор',
                self::CEO => 'Директор',
                self::MASTER => 'Мастер цеха',
                self::ACCOUNTANT => 'Бухгалтер',
                self::MARKETER => 'Маркетолог',
            ] :
            [
                self::ADMIN,
                self::CEO,
                self::MASTER,
                self::ACCOUNTANT,
                self::MARKETER,
            ];
    }

    /**
     * @param bool $withDescription
     * @return array
     */
    static function getAssignmentList(bool $withDescription = false)
    {
        $roles = self::getList($withDescription);
        unset($roles[self::ADMIN]);
        if (!\Yii::$app->user->can(Roles::ADMIN)) {
            unset($roles[self::CEO]);
        }

        return $roles;
    }
}