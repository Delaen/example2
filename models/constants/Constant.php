<?php

namespace app\models\constants;

/**
 * Class Constant
 * @package app\models\constants
 */
abstract class Constant
{
    const ORGANIZATION_SPM = 'org-spm';
    const ORGANIZATION_RT = 'org-rt';

    /**
     * @param bool $withDescription
     * @return array
     */
    static function organizations(bool $withDescription = false)
    {
        return $withDescription ?
            [
                self::ORGANIZATION_SPM => 'СПМ',
                self::ORGANIZATION_RT => 'ВШ',
            ] :
            [
                self::ORGANIZATION_SPM, self::ORGANIZATION_RT,
            ];
    }

    //Деление по цехам
    const WORKSHOP_ALL = 'all';
    const WORKSHOP_BARYSHEVO = 'baryshevo';
    const WORKSHOP_BARNAUL = 'barnaul';
    const WORKSHOP_VERH_TULA = 'verh-tula';

    /**
     * @param bool $withDescription
     * @return array
     */
    static function workshops(bool $withDescription = false)
    {
        return $withDescription ?
            [
                self::WORKSHOP_ALL => 'Все',
                self::WORKSHOP_BARYSHEVO => 'Барышево',
                self::WORKSHOP_BARNAUL => 'Барнаул',
                self::WORKSHOP_VERH_TULA => 'Верх-Тула',
            ] :
            [
                self::WORKSHOP_ALL,
                self::WORKSHOP_BARYSHEVO,
                self::WORKSHOP_BARNAUL,
                self::WORKSHOP_VERH_TULA,
            ];
    }

    /**
     * @param bool $withDescription
     * @return array
     */
    static function workshopsFilter(bool $withDescription = false)
    {
        return $withDescription ?
            [
                self::WORKSHOP_BARYSHEVO => 'Барышево',
                self::WORKSHOP_BARNAUL => 'Барнаул',
                self::WORKSHOP_VERH_TULA => 'Верх-Тула',
            ] :
            [
                self::WORKSHOP_BARYSHEVO,
                self::WORKSHOP_BARNAUL,
                self::WORKSHOP_VERH_TULA,
            ];
    }

    //Единицы измерения
    const UNIT_PIECE = 'piece';
    const UNIT_SQUARE_METER = 'square-meter';
    const UNIT_LINEAR_METER = 'linear-meter';
    const UNIT_KILOGRAM = 'kilogram';

    /**
     * @param bool $withDescription
     * @return array
     */
    static function units($withDescription = false)
    {
        return $withDescription ?
            [
                self::UNIT_PIECE => 'шт.',
                self::UNIT_SQUARE_METER => 'кв.м.',
                self::UNIT_LINEAR_METER => 'п.м.',
                self::UNIT_KILOGRAM => 'кг.'
            ] :
            [
                self::UNIT_PIECE, self::UNIT_SQUARE_METER, self::UNIT_LINEAR_METER, self::UNIT_KILOGRAM,
            ];
    }

    //Виды смен
    const DAY_SHIFT = 'day-shift';
    const NIGHT_SHIFT = 'night-shift';

    /**
     * @param bool $withDescription
     * @return array
     */
    static function workShifts($withDescription = false)
    {
        return $withDescription ?
            [
                self::DAY_SHIFT => 'Дневная смена',
                self::NIGHT_SHIFT => 'Ночная смена',
            ]:
            [
                self::DAY_SHIFT, self::NIGHT_SHIFT
            ];
    }

    //Типы производимой продукции
    const GOODS_TYPE_CRUMB = 'rubber-crumb';
    const GOODS_TYPE_TILE = 'tile';

    /**
     * @param bool $withDescription
     * @return array
     */
    static function workGoodsType($withDescription = false)
    {
        return $withDescription ?
            [
                self::GOODS_TYPE_CRUMB => 'Крошка',
                self::GOODS_TYPE_TILE => 'Плитка',
            ]:
            [
                self::GOODS_TYPE_CRUMB, self::GOODS_TYPE_TILE
            ];
    }

    //Фракции крошки
    const RUBBER_CRUMB_FRACTION_0_05 = 'rc-f-0-05';
    const RUBBER_CRUMB_FRACTION_0_08 = 'rc-f-0-08';
    const RUBBER_CRUMB_FRACTION_05_2 = 'rc-f-05-2';
    const RUBBER_CRUMB_FRACTION_1_3 = 'rc-f-1-3';
    const RUBBER_CRUMB_FRACTION_2_4 = 'rc-f-2-4';
    const RUBBER_CRUMB_FRACTION_3_5 = 'rc-f-3-5';

    /**
     * @param bool $withDescription
     * @return array
     */
    static function rubberCrumbFractions($withDescription = false)
    {
        return $withDescription ?
            [
                self::RUBBER_CRUMB_FRACTION_0_05 => '0-0,5',
                self::RUBBER_CRUMB_FRACTION_0_08 => '0-0,8',
                self::RUBBER_CRUMB_FRACTION_05_2 => '0,5-2',
                self::RUBBER_CRUMB_FRACTION_1_3 => '1-3',
                self::RUBBER_CRUMB_FRACTION_2_4 => '2-4',
                self::RUBBER_CRUMB_FRACTION_3_5 => '3-5',
            ]:
            [
                self::RUBBER_CRUMB_FRACTION_0_05, self::RUBBER_CRUMB_FRACTION_0_08, self::RUBBER_CRUMB_FRACTION_05_2,
                self::RUBBER_CRUMB_FRACTION_1_3, self::RUBBER_CRUMB_FRACTION_2_4, self::RUBBER_CRUMB_FRACTION_3_5
            ];
    }

    //Назначения платежей
    const PURPOSE_PAYMENT = 'payment';
    const PURPOSE_PRE_PAYMENT = 'pre-payment';
    const PURPOSE_OTHER = 'other';

    /**
     * @param bool $withDescription
     * @return array
     */
    static function purposes($withDescription = false)
    {
        return $withDescription ?
            [
                self::PURPOSE_PAYMENT => 'Предоплата',
                self::PURPOSE_PRE_PAYMENT => 'Оплата',
                self::PURPOSE_OTHER => 'Другое',
            ]:
            [
                self::PURPOSE_PAYMENT, self::PURPOSE_PRE_PAYMENT, self::PURPOSE_OTHER
            ];
    }

    //Способы оплаты
    const PAYMENT_TYPE_CASH = 'pt-cash';
    const PAYMENT_TYPE_CASHLESS = 'pt-cashless';
    const PAYMENT_TYPE_CARD = 'pt-card';

    /**
     * @param bool $withDescription
     * @return array
     */
    static function paymentTypes($withDescription = false)
    {
        return $withDescription ?
            [
                self::PAYMENT_TYPE_CASH => 'Наличные',
                self::PAYMENT_TYPE_CASHLESS => 'Безналичные',
                self::PAYMENT_TYPE_CARD => 'Карта',
            ]:
            [
                self::PAYMENT_TYPE_CASH, self::PAYMENT_TYPE_CASHLESS, self::PAYMENT_TYPE_CARD
            ];
    }

    //Flows
    const FLOW_RECEIPT = 'receipt';
    const FLOW_DEDUCTION = 'deduction';
}