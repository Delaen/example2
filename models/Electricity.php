<?php

namespace app\models;

use app\components\traits\CreatedUpdated;
use app\models\constants\Constant;
use Yii;

/**
 * This is the model class for table "electricity".
 *
 * @property int $id
 * @property string $date
 * @property int|null $reading_meter
 * @property string|null $comment
 * @property string $workshop
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property string|null $created
 * @property string|null $updated
 */
class Electricity extends \yii\db\ActiveRecord
{
    use CreatedUpdated;

    public $_used;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'electricity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['date', 'reading_meter', 'workshop'], 'required'],
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['reading_meter', 'created_at', 'updated_at'], 'integer'],
            [['reading_meter'], 'compare', 'operator' => '>', 'compareValue' => 0],
            [['comment'], 'string'],
            [['workshop'], 'in', 'range' => Constant::workshopsFilter()],
            [['reading_meter'], 'unique', 'targetAttribute' => ['reading_meter', 'workshop'], 'message' => 'Данные показания уже зафиксированы'],
            [['reading_meter'], 'validateReadingMeterMin'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'Id',
            'date' => 'Дата',
            'reading_meter' => 'Показания счетчика',
            'workshop' => 'Цех',
            'comment' => 'Примечание',
            'used' => 'Использовано за период',
            'created' => 'Добавлено',
            'updated' => 'Изменено',
        ];
    }

    public function beforeSave($insert)
    {
        $this->date && $this->date = date('Y-m-d', strtotime($this->date));
        return parent::beforeSave($insert);
    }

    public function afterFind()
    {
        $this->date && $this->date = date('d.m.Y', strtotime($this->date));
        parent::afterFind();
    }

    /**
     * Validates minimal value
     */
    public function validateReadingMeterMin()
    {
        $query = self::find()->andWhere(['workshop' => $this->workshop])
            ->andwhere([
                'or',
                [
                    'and',
                    ['>=', 'reading_meter', $this->reading_meter],
                    ['<=', 'date', date('Y-m-d', strtotime($this->date))]
                ],
               [
                   'and',
                   ['<=', 'reading_meter', $this->reading_meter],
                   ['>=', 'date', date('Y-m-d', strtotime($this->date))]
               ]
            ]);

        !$this->isNewRecord && $query->andWhere(['!=', 'id', $this->id]);

        if ($query->one()) {
            $this->addError('reading_meter', 'Неверное значения показаний');
        }
    }

    public function getUsed()
    {
        return $this->_used;
    }
}
