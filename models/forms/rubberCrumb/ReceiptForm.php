<?php

namespace app\models\forms\rubberCrumb;

use app\models\constants\Constant;
use app\models\flows\rubberCrumb\Receipt;
use Yii;
use yii\base\Model;

/**
 * @property string $date
 * @property float $fraction0_05
 * @property float $fraction0_08
 * @property float $fraction05_2
 * @property float $fraction1_3
 * @property float $fraction2_4
 * @property float $fraction3_5
 * @property string $comment
 * @property string $workshop
 *
 */
class ReceiptForm extends Model
{
    /**
     * @var string
     */
    public $date;
    /**
     * @var float
     */
    public $fraction0_05;
    /**
     * @var float
     */
    public $fraction0_08;
    /**
     * @var float
     */
    public $fraction05_2;
    /**
     * @var float
     */
    public $fraction1_3;
    /**
     * @var float
     */
    public $fraction2_4;
    /**
     * @var float
     */
    public $fraction3_5;
    /**
     * @var string
     */
    public $comment;
    /**
     * @var string
     */
    public $workshop;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['date', 'workshop'], 'required'],
            [['workshop'], 'in', 'range' => Constant::workshopsFilter()],
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['fraction0_05', 'fraction0_08', 'fraction05_2', 'fraction1_3', 'fraction2_4', 'fraction3_5'], 'number'],
            [['fraction0_05', 'fraction0_08', 'fraction05_2', 'fraction1_3', 'fraction2_4', 'fraction3_5'], 'filter', 'filter' => 'abs', 'skipOnEmpty' => true],
            [['comment'], 'string'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'date' => 'Дата',
            'workshop' => 'Цех',
            'fraction0_05' => '0-0,5',
            'fraction0_08' => '0-0,8',
            'fraction05_2' => '0,5-2',
            'fraction1_3' => '1-3',
            'fraction2_4' => '2-4',
            'fraction3_5' => '3-5',
            'comment' => 'Примечание',
        ];
    }

    /**
     *  Схораняет крошку
     * @param null $workShiftId
     */
    public function saveFractions($workShiftId = null)
    {
        $crumbs = [
            'fraction0_05' => Constant::RUBBER_CRUMB_FRACTION_0_05,
            'fraction0_08' => Constant::RUBBER_CRUMB_FRACTION_0_08,
            'fraction05_2' => Constant::RUBBER_CRUMB_FRACTION_05_2,
            'fraction1_3' => Constant::RUBBER_CRUMB_FRACTION_1_3,
            'fraction2_4' => Constant::RUBBER_CRUMB_FRACTION_2_4,
            'fraction3_5' => Constant::RUBBER_CRUMB_FRACTION_3_5,
        ];

        foreach ($crumbs as $crumbAttribute => $crumb) {
            if ($this->$crumbAttribute) {
                $receipt = new Receipt([
                    'work_shift_id' => $workShiftId,
                    'date' => $this->date,
                    'fraction' => $crumb,
                    'quantity' => $this->$crumbAttribute,
                    'workshop' => $this->workshop,
                    'comment' => $this->comment ?: null,
                ]);
                $receipt->save();
            }
        }
    }
}
