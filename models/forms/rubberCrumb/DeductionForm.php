<?php

namespace app\models\forms\rubberCrumb;

use app\helpers\StockHelper;
use app\models\constants\Constant;
use app\models\flows\rubberCrumb\Deduction;
use app\models\Order;
use Yii;
use yii\base\Model;
use yii\web\HttpException;

/**
 * @property string $date
 * @property float $fraction
 * @property float $fraction0_05
 * @property float $fraction0_08
 * @property float $fraction05_2
 * @property float $fraction1_3
 * @property float $fraction2_4
 * @property float $fraction3_5
 * @property int $order_id
 * @property string $comment
 * @property string $workshop
 *
 * @property Order $order
 */
class DeductionForm extends Model
{
    /**
     * @var string
     */
    public $date;
    /**
     * @var float
     */
    public $fraction;
    /**
     * @var float
     */
    public $fraction0_05;
    /**
     * @var float
     */
    public $fraction0_08;
    /**
     * @var float
     */
    public $fraction05_2;
    /**
     * @var float
     */
    public $fraction1_3;
    /**
     * @var float
     */
    public $fraction2_4;
    /**
     * @var float
     */
    public $fraction3_5;
    /**
     * @var int
     */
    public $order_id;
    /**
     * @var string
     */
    public $comment;
    /**
     * @var Order|null
     */
    public $_order = null;
    /**
     * @var string
     */
    public $workshop;

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['date', 'workshop'], 'required'],
            [['workshop'], 'in', 'range' => Constant::workshopsFilter()],
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            [['fraction', 'fraction0_05', 'fraction0_08', 'fraction05_2', 'fraction1_3', 'fraction2_4', 'fraction3_5'], 'number'],
            [['fraction', 'fraction0_05', 'fraction0_08', 'fraction05_2', 'fraction1_3', 'fraction2_4', 'fraction3_5'], 'filter', 'filter' => 'abs', 'skipOnEmpty' => true],
            [['comment'], 'string'],
            [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
        ];
    }

    public function attributeLabels()
    {
        return [
            'date' => 'Дата',
            'order_id' => 'Заказ',
            'fraction' => 'Количество',
            'workshop' => 'Цех',
            'fraction0_05' => '0-0,5',
            'fraction0_08' => '0-0,8',
            'fraction05_2' => '0,5-2',
            'fraction1_3' => '1-3',
            'fraction2_4' => '2-4',
            'fraction3_5' => '3-5',
            'comment' => 'Примечание',
        ];
    }

    /**
     * @return Order|null
     */
    public function getOrder()
    {
        if (!$this->_order && $this->order_id) {
            $this->_order = Order::findOne($this->order_id);
        }

        return $this->_order;
    }

    /**
     * Отгружаем крошку
     */
    public function saveFractions()
    {
        if ($this->fraction && ($this->fraction0_1 || $this->fraction1_3 || $this->fraction3_5)) {
            throw new HttpException(400, 'Ошибка заполнения формы');
        }

        if ($this->order_id && $this->fraction) {
            if (!$this->order) {
                throw new HttpException(400, 'Неверно указан заказ');
            }

            if ($this->fraction > $this->order->shippingDebt) {
                $this->addError('fraction', 'Указанное количество больше остатка по заказу');
                return false;
            }

            $deduction = new Deduction([
                'date' => $this->date,
                'order_id' => $this->order_id,
                'fraction' => $this->order->fraction,
                'quantity' => $this->fraction,
                'workshop' => $this->workshop,
                'comment' => $this->comment,
            ]);
            if (!$deduction->save()) {
                $this->addError('fraction', $deduction->errors[key($deduction->errors)][0]);
                return false;
            }

        } else {
            $crumbs = [
                'fraction0_05' => Constant::RUBBER_CRUMB_FRACTION_0_05,
                'fraction0_08' => Constant::RUBBER_CRUMB_FRACTION_0_08,
                'fraction05_2' => Constant::RUBBER_CRUMB_FRACTION_05_2,
                'fraction1_3' => Constant::RUBBER_CRUMB_FRACTION_1_3,
                'fraction2_4' => Constant::RUBBER_CRUMB_FRACTION_2_4,
                'fraction3_5' => Constant::RUBBER_CRUMB_FRACTION_3_5,
            ];
            foreach ($crumbs as $crumbAttribute => $crumb) {
                if($this->$crumbAttribute) {
                    $deduction = new Deduction([
                        'date' => $this->date,
                        'fraction' => $crumb,
                        'quantity' => $this->$crumbAttribute,
                        'workshop' => $this->workshop,
                        'comment' => $this->comment,
                    ]);
                    if (!$deduction->save()) {
                        $this->addError($crumbAttribute, $deduction->errors[key($deduction->errors)][0]);
                        return false;
                    }
                }
            }
        }

        return true;
    }
}
