<?php

namespace app\models\forms\rubberCrumb;

use app\models\constants\Constant;
use app\models\flows\rubberCrumb\Receipt;
use app\models\flows\rubberCrumb\Salary;
use app\models\WorkShift;
use yii\base\Model;
use Yii;

/**
 * @property string $date
 * @property float $type
 * @property float $fraction0_05
 * @property float $fraction0_08
 * @property float $fraction05_2
 * @property float $fraction1_3
 * @property float $fraction2_4
 * @property float $fraction3_5
 * @property array $salaries
 *
 */
class WorkShiftForm extends Model
{
    /**
     * @var string
     */
    public $date;
    /**
     * @var string
     */
    public $workshop;
    /**
     * @var string
     */
    public $type;
    /**
     * @var float
     */
    public $fraction0_05;
    /**
     * @var float
     */
    public $fraction0_08;
    /**
     * @var float
     */
    public $fraction05_2;
    /**
     * @var float
     */
    public $fraction1_3;
    /**
     * @var float
     */
    public $fraction2_4;
    /**
     * @var float
     */
    public $fraction3_5;
    /**
     * @var array
     */
    public $salaries = [];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['date', 'type', 'workshop'], 'required'],
            [['type'], 'in', 'range' => Constant::workShifts()],
            [['workshop'], 'in', 'range' => Constant::workshopsFilter()],
            [['fraction0_05', 'fraction0_08', 'fraction05_2', 'fraction1_3', 'fraction2_4', 'fraction3_5'], 'number'],
            [['fraction0_05', 'fraction0_08', 'fraction05_2', 'fraction1_3', 'fraction2_4', 'fraction3_5'], 'filter', 'filter' => 'abs', 'skipOnEmpty' => true],
            [['date'], 'date', 'format' => 'php:d.m.Y'],
            ['salaries', 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Дата',
            'type' => 'Вид смены',
            'workshop' => 'Цех',
            'fraction0_05' => '0-0,5',
            'fraction0_08' => '0-0,8',
            'fraction05_2' => '0,5-2',
            'fraction1_3' => '1-3',
            'fraction2_4' => '2-4',
            'fraction3_5' => '3-5',
        ];
    }

    /**
     * Saves work shift
     * @return bool|int
     * @throws \yii\db\Exception
     */
    public function save()
    {
        $workShift = new WorkShift();
        $workShift->load($this->attributes, '');

        if (!$this->fraction0_05 && !$this->fraction0_08 && !$this->fraction05_2 &&
            !$this->fraction1_3 && !$this->fraction2_4 && !$this->fraction3_5) {
            $this->addError('fraction0_05', 'Укажите количество изготовленной крошки');
            $this->addError('fraction0_08', 'НУкажите количество изготовленной крошки');
            $this->addError('fraction05_2', 'Укажите количество изготовленной крошки');
            $this->addError('fraction1_3', 'Укажите количество изготовленной крошки');
            $this->addError('fraction2_4', 'Укажите количество изготовленной крошки');
            $this->addError('fraction3_5', 'Укажите количество изготовленной крошки');

            return false;
        }

        $transaction = Yii::$app->db->beginTransaction();
        if ($workShift->save()) {
            if ($this->saveRubberCrumbs($workShift->id)) {
                if ($this->saveSalaries($workShift->id)) {
                    $transaction->commit();
                    return $workShift->id;
                }

                $this->addError('salaries', 'Неверно заполнены данные по з/п');
                return false;
            }

            $this->addError('fraction0_05', 'Неверно заполнены данные по крошке');
            $this->addError('fraction0_08', 'Неверно заполнены данные по крошке');
            $this->addError('fraction05_2', 'Неверно заполнены данные по крошке');
            $this->addError('fraction1_3', 'Неверно заполнены данные по крошке');
            $this->addError('fraction2_4', 'Неверно заполнены данные по крошке');
            $this->addError('fraction3_5', 'Неверно заполнены данные по крошке');
            return false;
        }

       $this->addError(key($workShift->errors), $workShift->errors[key($workShift->errors)][0]);
       return false;
    }

    /**
     * Saves rubber crumb receipts
     * @param $workShiftId
     * @return bool
     */
    protected function saveRubberCrumbs($workShiftId)
    {
        foreach (['fraction0_05', 'fraction0_08', 'fraction05_2', 'fraction1_3', 'fraction2_4', 'fraction3_5'] as $fraction) {
            if ($this->$fraction) {
                $receipt = new Receipt([
                    'work_shift_id' => $workShiftId,
                    'date' => $this->date,
                    'fraction' => $this->getFractionType($fraction),
                    'quantity' => $this->$fraction,
                    'workshop' => $this->workshop,
                ]);

                if (!$receipt->save()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Saves salaries
     * @param $workShiftId
     * @return bool
     */
    protected function saveSalaries($workShiftId)
    {
        if (is_array($this->salaries)) {
            foreach ($this->salaries as $salary) {
                $salaryModel = new Salary([
                    'employee_id' => $salary['employee_id'],
                    'work_shift_id' => $workShiftId,
                    'salary' => $salary['salary'],
                    'additional_work_id' => $salary['additional_work_id'],
                    'additional_work_salary' => $salary['additional_work_salary'],
                    'comment' => $salary['comment'],
                ]);

                if (!$salaryModel->save()) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param string $fraction
     * @return string|null
     */
    protected function getFractionType(string $fraction): ?string
    {
        switch ($fraction) {
            case 'fraction0_05':
                return Constant::RUBBER_CRUMB_FRACTION_0_05;
            case 'fraction0_08':
                return Constant::RUBBER_CRUMB_FRACTION_0_08;
            case 'fraction05_2':
                return Constant::RUBBER_CRUMB_FRACTION_05_2;
            case 'fraction1_3':
                return Constant::RUBBER_CRUMB_FRACTION_1_3;
            case 'fraction2_4':
                return Constant::RUBBER_CRUMB_FRACTION_2_4;
            case 'fraction3_5':
                return Constant::RUBBER_CRUMB_FRACTION_3_5;
        }

        return null;
    }
}
