<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Electricity as ElectricityModel;

/**
 * Electricity represents the model behind the search form of `app\models\Electricity`.
 */
class ElectricitySearch extends ElectricityModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'reading_meter', 'created_at', 'updated_at'], 'integer'],
            [['date', 'comment', 'workshop'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ElectricityModel::find()
            ->select([
                'id',
                'date',
                'reading_meter',
                '_used' => 'reading_meter - IFNULL((' . self::find()
                        ->select(['max' => 'max(reading_meter)'])->alias('t2')
                        ->where('t2.reading_meter < ' . self::tableName() . '.reading_meter')
                        ->andWhere('t2.workshop = ' . self::tableName() . '.workshop')
                        ->createCommand()->rawSql . '), 0)',
                'comment'])
            ->orderBy('date DESC');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'date' => $this->date,
            'reading_meter' => $this->reading_meter,
            'workshop' => $this->workshop,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
