<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Cost as CostModel;
use Yii;

/**
 * Cost represents the model behind the search form of `app\models\Cost`.
 */
class CostSearch extends CostModel
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'cost_catalog_id', 'created_at', 'updated_at'], 'integer'],
            [['date', 'comment', 'workshop'], 'string'],
            [['quantity'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CostModel::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => false
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->date) {
            $query->andWhere(['date' => date('Y-m-d', strtotime($this->date))]);
        }

        if (Yii::$app->user->identity->getRole() == \app\models\constants\Roles::MASTER) {
            $this->workshop = Yii::$app->user->identity->user->workshop;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'cost_catalog_id' => $this->cost_catalog_id,
            'quantity' => $this->quantity,
            'workshop' => $this->workshop,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'comment', $this->comment]);

        return $dataProvider;
    }
}
