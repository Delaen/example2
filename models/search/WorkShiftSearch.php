<?php

namespace app\models\search;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\WorkShift;
use Yii;

/**
 * WorkShiftSearch represents the model behind the search form of `app\models\WorkShift`.
 */
class WorkShiftSearch extends WorkShift
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'created_at', 'updated_at'], 'integer'],
            [['date', 'type', 'workshop'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = WorkShift::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (Yii::$app->user->identity->getRole() == \app\models\constants\Roles::MASTER) {
            $this->workshop = Yii::$app->user->identity->user->workshop;
        }

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        if ($this->date) {
            $query->andWhere(['date' => date('Y-m-d', strtotime($this->date))]);
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'workshop' => $this->workshop,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ]);

        $query->andFilterWhere(['like', 'type', $this->type]);

        return $dataProvider;
    }
}
